
<?php


require_once "config.php";
require_once "game_config.php";

$gameNo = 0;
$clueNo = date("d-m-Y");

$noOfClues =1;

$clueString = constant("GAME".$gameNo."_CLUETEXT".$clueNo);
$clueTitle = constant("GAME".$gameNo."_CLUETITLE".$clueNo);
$clueImage = constant("GAME".$gameNo."_CLUEIMAGE".$clueNo);
$clueType = constant("GAME".$gameNo."_CLUETYPE".$clueNo);
$hint = constant("GAME".$gameNo."_HINT".$clueNo);
$gameTitle = constant("GAME".$gameNo."_CLUETITLE0");
$gameDesc = constant("GAME".$gameNo."_CLUETEXT0");

$hint = "Hint :".$hint ;
/* Handling the daily logic . Need to check if the gameNo says daily and if so , dont set the user session 
( people can play dailies annonymously). 

*/


// Populate the data around the clue based on what type it is - Video , Audio, Image or Text

if ($clueType == "Image"){ 
    $divValue =  "<img src=\"".$clueImage."\" class=\" col-lg-12 img-fluid\">" ;
}
else if ($clueType == "Audio") {
  $divValue =  "<div class=\"card\"><audio controls src=\"".$clueImage."\"> Your browser does not support the <code>audio</code> element. </audio></div>" ;
}
else if ($clueType == "Video") {
  $divValue =  "<div class=\"card\"><video class=\"responsive-video\" controls><source src=\"".$clueImage."\" type=\"video/mp4\"></video></div>" ;
}
else if ($clueType == "Text") {
  $divValue =  "<h1 class=\"display-4\">".$clueImage."</h1>" ;
}
else {
	$divValue = "<div class=\"card-image\"> <img src=\"".$clueImage."\"> <span class=\"card-title\">Clue 7</span></div> " ;
}

// incremenet the next clue 
$nextClue =$clueNo +1 ;
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Welcome to Mimir</title>
	<!-- aos -->
	<link href="css/aos.css" rel="stylesheet">	
	<!-- bootstrap -->
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<!-- style.css -->
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>

	<header>
		<nav class="navbar navbar-expand navbar-light">
			<!-- Brand/logo -->
			<a class="navbar-brand logo" href="index.html">
				<img src="images/logo.gif" alt="logo">
			</a>

			<div class="logo_text">
				<p class="py-2 py-lg-0">Where the brain goes to have FUN</p>
			</div>
			<ul class="py-1 py-lg-0 navbar-nav ml-auto">
				<li class="nav-item">
					<a class="nav-link icon_link login" href="login_game.php">
						<img src="images/enter.png" class="d-block d-lg-none"> Login
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link icon_link" href="#">
						<img src="images/Path 7.png">
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link icon_link" href="#">
						<img src="images/noun_menu_2310205.png">
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link icon_link" href="#">
						<img src="images/noun_redo_2840884.png">
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link icon_link" href="#">
						<img src="images/noun_menu_521074.png">
					</a>
				</li>
			</ul>
		</nav>
	</header>
	<!-- about -->
	<section class="about_section">
		<div class="container">
			<div class="about_text" data-aos="fade-up">
				<h5>Mimir</h5>
				<p>	Mimir means the rememberer. Beneath the roots of the world tree Yggdrasil there lies a well called Mimirbrunnr, filled with all the wisdom in the world, named after the wise god Mimir who resides there.</p>
			</div>
		</div>
    </section>


	<!-- cards -->
	<section class="cards_section">
		<div class="container">
			<div class="row">
				<!-- card1 starts -->
				<div class="col-md-12 col-lg-6">
					<div class="cards" data-aos="flip-left">
						<div class="form-row flex-sm-row-reverse">
							<div class="col-sm-6">
								<div class="card_image">
									<img src="images/G1C0image.jpg">
								</div>
							</div>
							<div class="col-sm-6 d-flex align-items-center">
								<div class="card_text">
									<h6>Mystery of the Missing Ambassador</h6>
									<p>An ambassador from the country of Gernobia has gone missing and Scotland Yard is asking for your help</p>
									<a href="game_1_page.html" class="yellow_color">Play The Game Now</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- card1 ends -->
				<!-- card2 starts -->
				<div class="col-md-12 col-lg-6 margin_top mt-lg-0">
					<div class="cards" data-aos="flip-left">
						<div class="form-row flex-sm-row-reverse">
							<div class="col-sm-6">
								<div class="card_image">
									<img src="images//G2C0.jpg">
								</div>
							</div>
							<div class="col-sm-6 d-flex align-items-center">
								<div class="card_text">
									<h6>Velkommen til Oslo!</h6>
									<p>Welcome to Oslo! This is going to be an exciting journey around the capital city of Norway. We’ll take you to some interesting places and some really cool stories!</p>
									<a href="game_2_page.html" class="yellow_color">Play The Game Now</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- card2 ends -->
				<!-- card3 starts -->
				<div class="col-md-12 col-lg-6 margin_top">
					<div class="cards" data-aos="flip-left">
						<div class="form-row flex-sm-row-reverse">
							<div class="col-sm-6">
								<div class="card_image">
									<img src="images/G3C0Image.jpg">
								</div>
							</div>
							<div class="col-sm-6 d-flex align-items-center">
								<div class="card_text">
									<h6>Build Your Story!</h6>
									<p>Are you a story teller? Here is your chance to make a story while solving a movie mimir.</p>
									<a href="game_3_page.html" class="yellow_color">Play The Game Now</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- card3 ends -->
				<!-- card4 starts -->
				<div class="col-md-12 col-lg-6 margin_top">
					<div class="cards" data-aos="flip-left">
						<div class="form-row flex-sm-row-reverse ">
							<div class="col-sm-6">
								<div class="card_image">
									<img src="images/recipe.jpg">
								</div>
							</div>
							<div class="col-sm-6 d-flex align-items-center">
								<div class="card_text ">
									<h6>Michelin Star Chef Game</h6>
									<p>Identify the key ingredients of an interesting dish in this six clue game</p>
									<a href="game_4_page.html" class="yellow_color">Play The Game Now</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			<!-- card4 ends -->
				<!-- card5 starts -->
				<div class="col-md-12 col-lg-6 margin_top">
					<div class="cards" data-aos="flip-left">
						<div class="form-row flex-sm-row-reverse ">
							<div class="col-sm-6">
								<div class="card_image">
									<img src="images/G5C0image.jpg">
								</div>
							</div>
							<div class="col-sm-6 d-flex align-items-center">
								<div class="card_text ">
									<h6>The Mimir Sports Challenge</h6>
									<p>So you think you are a sports buff? Our Professor at Mimir challenges you to take this sports quiz. Identify the seven champions in their respective sporting disciplines and win the Mimir Sports Cup!</p>
									<a href="game_5_page.html" class="yellow_color">Play The Game Now</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- card5 ends -->
				<!-- card6 starts -->
				<div class="col-md-12 col-lg-6 margin_top">
					<div class="cards" data-aos="flip-left">
						<div class="form-row flex-sm-row-reverse ">
							<div class="col-sm-6">
								<div class="card_image">
									<img src="images/G6C0image.jpg">
								</div>
							</div>
							<div class="col-sm-6 d-flex align-items-center">
								<div class="card_text ">
									<h6>The Mimir Movie Quiz</h6>
									<p>Fancy yourself as a movie buff? Why dont you play this game and find out if you are a champion?</p>
									<a href="game_6_page.html" class="yellow_color">Play The Game Now</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- card6 ends -->
				<!-- next card can start here -->
				<!-- card7 starts -->
				<div class="col-md-12 col-lg-6 margin_top">
					<div class="cards" data-aos="flip-left">
						<div class="form-row flex-sm-row-reverse ">
							<div class="col-sm-6">
								<div class="card_image">
									<img src="images/G7C0image.jpg">
								</div>
							</div>
							<div class="col-sm-6 d-flex align-items-center">
								<div class="card_text ">
									<h6>Play with Greek Gods!!</h6>
									<p>Find the names of Greek Gods and Godesses in this game. You dont need to be an expert in Greek mythology though it will help you along</p>
									<a href="game_7_page.html" class="yellow_color">Play The Game Now</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- card7 ends -->
				<!-- next card can start here -->

				<!-- next card can end here -->

				<div class="view_btn text-center" data-aos="zoom-in">
					<a href="#" class="yellow_color ">View More <img src="images/Group 13.png" class="ml-3"></a>
				</div>
			</div>
		</div>
	</section>
	<!-- footer -->
	<footer class="footer_section">
		<div class="container">
			<div class="row">
				<div class="col-12 col-sm-6">
					<div class="text-center text-sm-left">
						<p class="footer_text text-white">© 2020 Mimir Copyright </p>
					</div>
				</div>
				<div class="col-12 col-sm-6">
					<div class="text-center text-sm-right mt-2 mt-sm-0">
						<a href="#" class="footer_text text-white">More Links</a>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<!-- bootstrap -->
	<script src="js/jquery.min.js"></script>
	<script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/processor.js"></script>
    <script src="js/timer.js"></script>
	<!-- aos -->
	<script src="js/aos.js"></script>
	<script type="text/javascript">
		AOS.init();
	</script>
</body>
</html>