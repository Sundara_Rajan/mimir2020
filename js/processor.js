$(document).ready(function () {

  $('#hintButton').hide();
  $('#hintText').hide();
  $('#nextButton').hide();
  $('#backHomeButton').hide();
 
  //checkFlag =sessionStorage.getItem('timeNow');

  //if(checkFlag == undefined){   // some flag was stored
   
    //$('#answers').hide();
   
    //Cookies.set('firstAttempt', "flag", { expires: 40/1440 }); // store cookie
  //}

  //else {
    //$('#answers').show();
  //}
  


  //alert($clueString);

// Executes when the HTML document is loaded and the DOM is ready
seconds = sessionStorage.getItem('timeNow');

  if(!seconds) {  
    seconds  = 0; 
  }

  document.getElementById("timer").innerHTML = "<h4>"+seconds+"  Minutes</h4>";

});

 $("#cluebutton1").on("click", function(event) {


  event.preventDefault();

  x = document.getElementById("textarea1").value;
  console.log(x);
  if (x == "") {
    console.log("No x value");
      alert("Whatttt ? Entering no value and hitting enter? Hvorfor det , hæ?");
      return false;
  }

    
          
    
    // find out which elemnt was changed    
    var $post = {};
    $post.answer = $('#textarea1').val();
    $post.clueNo = $('#clueNo').val();
    $post.gameNo = $('#gameNo').val();
    $post.user = $('#user').val();
    console.log($post.user);
    $post.seconds = seconds;
    $post.noOfClues =$('#noOfClues').val();

    //console.log("The answer is  value :"+$post.answer);
    //console.log($post);
    
    $.ajax({
        url: 'cluechecker.php',
        type: 'POST',
        data: $post,
        cache: false,
        success: function (data) {
        //alert('Your data updated');
        //location.reload();
        // code that calls updatetable.php
          console.log(data);

          if (data == "No"){

             // check if there is a flag set in the cookie for the attempted answers .
            // if the cookie is not set , set to be true the first . If true , append to existing answer
            storedFlag = sessionStorage.getItem('firstAttempt'); // read cookie
           
            //Commented out the use of sessionstorage to decide whether the answer box shouldbe shown or not
            //if(storedFlag == undefined){   // some flag was stored
              //$('#answers').show();
              $("#answers").append("<b>"+$post.answer+" is incorrect</b><br>")

              //sessionStorage.setItem('firstAttempt', "true");

              
              
              //Cookies.set('firstAttempt', "flag", { expires: 40/1440 }); // store cookie
            // }

           // else {
             
             // $("#answers").append("<b>"+$post.answer+" is incorrect</b><br>")
           // }

            //console.log("The answer is wrong");
            $('#hintButton').show();

            $("#textarea1").val(" ")


          }

          else {
          
            var returnedData = JSON.parse(data);

            //console.log (returnedData);

            // set the data into content of row3
            $answerImage = returnedData["answerImage"];

            $("#clueImg1").html("<img src=\""+$answerImage+"\" class=\"img-fluid\">");

            $quoteToAdd = returnedData["answerQuote"];
            $triviaToAdd = returnedData["trivia"] ;
            $nextFileName = returnedData["nextFileName"] ;
            $formalAnswer = returnedData["formalAnswer"] ;

            $answerUpdate = "<h5>"+ $formalAnswer +" is the right answer !!</h5><p>"

            $("#textForClue").html($answerUpdate+$quoteToAdd+"<p>"+ "<b> <font size = \"5\" color = \"#0000CD\">Fun fact:  <br>"+ $triviaToAdd+"</font></b><br>");
            $('#cluechecker').hide();
            $('#hintText').hide();
            $('#textarea1').hide();
            
            $("#textForCard").append($answerUpdate);

            $("#NextButton1").attr("action", $nextFileName);

            console.log($post.noOfClues );
            console.log($post.clueNo );
            sessionStorage.removeItem('firstAttempt');


            // check total clues to the currrent clue n to decide if this is the final clue
            if ($post.noOfClues > $post.clueNo){
              $('#nextButton').show();

            }

            else {
              finalTime = sessionStorage.getItem('timeNow');
              clearInterval(intervalStop);
              document.getElementById("timer").innerHTML = "Your final time is "+finalTime+ " minutes";
              sessionStorage.setItem('timeNow', 0);
              $('#backHomeButton').show();
            }


            window.scrollBy(0,80);


          }

        },
        error: function () {
            //alert('error handing here');
        }
    });
  });

 
// PROCESS THE CASE WHERE THE ANSWER IS ASKED FOR WITHOUT 
$("#skipToAnswer").on("click", function(event) {


  event.preventDefault();

  // No answer - take off later
  x = "skipToAnswer";
              
    // find out which elemnt was changed    
    var $post = {};
    $post.answer = "skipToAnswer";
    $post.clueNo = $('#clueNo').val();
    $post.gameNo = $('#gameNo').val();
    $post.user = $('#user').val();
    //console.log($post.user);
    $post.seconds = seconds;
    $post.noOfClues =$('#noOfClues').val();

    //console.log("The answer is  value :"+$post.answer);
    //console.log($post);
    
    $.ajax({
        url: 'cluechecker.php',
        type: 'POST',
        data: $post,
        cache: false,
        success: function (data) {
        //alert('Your data updated');
        //location.reload();
        // code that calls updatetable.php
          console.log(data);

          if (data == "No"){

             // check if there is a flag set in the cookie for the attempted answers .
            // if the cookie is not set , set to be true the first . If true , append to existing answer
            storedFlag = sessionStorage.getItem('firstAttempt'); // read cookie
           
            //Commented out the use of sessionstorage to decide whether the answer box shouldbe shown or not
            //if(storedFlag == undefined){   // some flag was stored
              //$('#answers').show();
              $("#answers").append("<b>"+$post.answer+" is incorrect</b><br>")
            $('#hintButton').show();

            $("#textarea1").val(" ")


          }

          else {
          
            var returnedData = JSON.parse(data);

            //console.log (returnedData);

            // set the data into content of row3
            $answerImage = returnedData["answerImage"];

            $("#clueImg1").html("<img src=\""+$answerImage+"\" class=\"img-fluid\">");

            $quoteToAdd = returnedData["answerQuote"];
            $triviaToAdd = returnedData["trivia"] ;
            $nextFileName = returnedData["nextFileName"] ;
            $formalAnswer = returnedData["formalAnswer"] ;

            $answerUpdate = "<h5>"+ $formalAnswer +" is the right answer !!</h5><p>"

            $("#textForClue").html($answerUpdate+$quoteToAdd+"<p>"+ "<b> <font size = \"5\" color = \"#0000CD\">Fun fact:  <br>"+ $triviaToAdd+"</font></b><br>");
            $('#cluechecker').hide();
            $('#hintText').hide();
            $('#textarea1').hide();
            
            $("#textForCard").append($answerUpdate);

            $("#NextButton1").attr("action", $nextFileName);

            console.log($post.noOfClues );
            console.log($post.clueNo );
            sessionStorage.removeItem('firstAttempt');


            // check total clues to the currrent clue n to decide if this is the final clue
            if ($post.noOfClues > $post.clueNo){
              $('#nextButton').show();

            }

            else {
              finalTime = sessionStorage.getItem('timeNow');
              clearInterval(intervalStop);
              document.getElementById("timer").innerHTML = "Your final time is "+finalTime+ " minutes";
              sessionStorage.setItem('timeNow', 0);
              $('#backHomeButton').show();
            }


            window.scrollBy(0,80);


          }

        },
        error: function () {
            //alert('error handing here');
        }
    });
  });



  
 
 
  function getHint(){

    if ($('#hintText').is(':hidden')) {

      $('#hintText').show();
      document.getElementById('hintText').innerHTML = $('#hint').val();

    } 
   
  };


function timeTheGame (){
  seconds++;

  document.getElementById("timer").innerHTML ="<h4>"+seconds+" Minutes</h4>";


  sessionStorage.setItem('timeNow', seconds);

}

intervalStop  = window.setInterval(timeTheGame ,60000);


function required() {
  var x;
  x = document.getElementById("textarea1").value;
  console.log(x);
  if (x == "") {
    console.log("No x value");
      alert("Enter an Answer");
      return false;
  }
}




