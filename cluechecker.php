<?php

//echo "$_POST";
require_once "config.php";
require_once "game_config.php";

//$myfile = fopen("logs.txt", "wr") or die("Unable to open file!");

    $servername = DB_SERVER;
    $username = DB_NAME;
    $password = DB_PASSWORD;
    $dbname = DB_NAME;

     

    // make a hash out of the server params
    $serverParams ["servername"] = $servername;
    $serverParams ["username"] = $username;
    $serverParams ["password"] = $password;
    $serverParams ["dbname"] = $dbname;

    // game 3 deatils
    $gameThreeClueNo = 6;

    // get the post data

    $answer_given = htmlspecialchars($_POST["answer"]);
    $clueNo = htmlspecialchars($_POST["clueNo"]);
    $gameNo = htmlspecialchars($_POST["gameNo"]);
    $uname = htmlspecialchars($_POST["user"]);
    $seconds = htmlspecialchars($_POST["seconds"]);


    //fwrite($myfile,$answer_given." : the answer value\n");

    $returnVal = processClueResult ($gameNo,$clueNo,$answer_given);
    //fwrite($myfile,$returnVal." : the return value\n");
    //fwrite($myfile,$uname." : user value\n");
    //fwrite($myfile,$clueNo." : the clue value\n");
    //fwrite($myfile,$gameNo." : the game value\n");
  
    // set the flag for right or wrong asnwer
    if ($returnVal === "No"){
      $rightAnswer = "N";
    } 
    else {
      $rightAnswer = "Y";
    }

    // Create the insert statement
    $insertForAnswer = "INSERT INTO `game_plays`( `GameNo`, `ClueNo`, `UserID`, `timetaken`, `RightAnswer`,`AnswerGiven`,`Comments`) VALUES (".$gameNo." ,".$clueNo.",(select id from users where username=\"".$uname."\" ),".$seconds.",\"".$rightAnswer."\", \"".$answer_given."\", '')" ;

    //fwrite($myfile,$insertForAnswer."insert sql\n");

    // open DB connection and run the query
    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    //Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
        
    }

    $result = $conn->query($insertForAnswer);

    //fclose($myfile);
    echo $returnVal;




  function processClueResult ($gameNo, $clueNo,$answertoCheck) {
    // create a flag for correct answer 
    $isRightAnswer = "N";

    //build the names 
    $answerString = constant("GAME".$gameNo."_ANSWER".$clueNo);
    $trivia = constant("GAME".$gameNo."_TRIVIA".$clueNo);
    $answerImage = constant("GAME".$gameNo."_ANSWERIMAGE".$clueNo);
    $answerQuote = constant("GAME".$gameNo."_ANSWERQUOTE".$clueNo);
    $formalAnswer = constant("GAME".$gameNo."_FORMALANSWER".$clueNo);
    $nextFile = constant("GAME".$gameNo."_NEXTFILE".$clueNo);
    $hintToShow =  constant("GAME".$gameNo."_HINT".$clueNo);
    $answerStringArray = explode(";",$answerString);


    if ($answertoCheck === "skipToAnswer" ){

        // skip processing just give the answer
        $isRightAnswer = "Y";

    }

    else {
  
      foreach ($answerStringArray as $answerString1) {
        $answer_given = preg_replace("/\s+/", "", $answertoCheck);
        $clue = "/$answerString1/i";
      
        if (preg_match($clue,$answer_given)) {
          $isRightAnswer = "Y";
          break;
        }
      
      }

    }
   
    if ($isRightAnswer ==="Y"){
      //if the ans is correct return the values forthis clue
       $clueData["answer"]=  $answerString1 ;
       $clueData["trivia"]= $trivia ;      
       $clueData["answerImage"]= $answerImage ;
       $clueData["answerQuote"]= $answerQuote ;
       $clueData["answerGiven"]= $answertoCheck ;
       $clueData["nextFileName"]= $nextFile ;
       $clueData["formalAnswer"]= $formalAnswer ;
       return json_encode($clueData);

    }
    else {
      return "No";
    }

  }


?>