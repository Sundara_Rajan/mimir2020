<?php

/**
 * @file
 *
 * Reads data from a Google Spreadsheet that needs authentication.
 */

require 'vendor/autoload.php';

/**
 * Set here the full path to the private key .json file obtained when you
 * created the service account. Notice that this path must be readable by
 * this script.
 */
$service_account_file = 'active-complex-207113-319ed366057e.json';

/**
 * This is the long string that identifies the spreadsheet. Pick it up from
 * the spreadsheet's URL and paste it below.
 */
$spreadsheet_id = '1Dqs859oTtSgM05BO4AIS8DF53GayCwwzWB_z7I1OEoA';

/**
 * This is the range that you want to extract out of the spreadsheet. It uses
 * A1 notation. For example, if you want a whole sheet of the spreadsheet, then
 * set here the sheet name.
 *
 * @see https://developers.google.com/sheets/api/guides/concepts#a1_notation
 * define('GAME3_CLUEIMAGE1', 'images/Clue1.jpg');
*define('GAME3_CLUETEXT1', 'Find this famous sports personality by using the composite image that is your clue . Your time has started');
*define('GAME3_CLUETITLE1', 'images/clue1p2.jpg');
*define('GAME3_ANSWER1', 'kapildev;kapil');
*define('GAME3_TRIVIA1', 'Kapil Dev has never been run out in his 184 Test innings long career.');
*define('GAME3_ANSWERIMAGE1', 'images/clue1p2.jpg');
*define('GAME3_ANSWERQUOTE1', 'Puttar..well done. But I play golf these days. What will I do with a cup?I suggest you take a flight to Europe and see if someone is playing football there…');
*define('GAME3_NEXTFILE1', 'mainProcessor.php');
* define('GAME3_FORMALANSWER1', 'Kapil Dev');
* define('GAME3_CLUETYPE1', 'Image');
 * 
 * GameNo	ClueNo	ClueImageName	ClueText	ClueTitle	Answer	trivia	AnswerImageName	AnswerQuote	FormalAnswer	ClueType
 * 
 */
$spreadsheet_range = 'A1:L57';

putenv('GOOGLE_APPLICATION_CREDENTIALS=' . $service_account_file);
$client = new Google_Client();
$client->useApplicationDefaultCredentials();
$client->addScope(Google_Service_Sheets::SPREADSHEETS_READONLY);
$service = new Google_Service_Sheets($client);

$result = $service->spreadsheets_values->get($spreadsheet_id, $spreadsheet_range);

$myfile = fopen("gsheetConfig.txt", "w");

foreach ($result as $v1) {
        echo "define ('GAME$v1[0]_CLUEIMAGE$v1[1]', 'images/$v1[2]');\n";

        fwrite($myfile, "define ('GAME$v1[0]_CLUEIMAGE$v1[1]', 'images/$v1[2]');\n");
        echo "define ('GAME$v1[0]_CLUETEXT$v1[1]', '$v1[3]');\n";
        fwrite($myfile,"define ('GAME$v1[0]_CLUETEXT$v1[1]', '$v1[3]');\n");
        echo "define ('GAME$v1[0]_CLUETITLE$v1[1]', '$v1[4]');\n";
        fwrite($myfile,"define ('GAME$v1[0]_CLUETITLE$v1[1]', '$v1[4]');\n");
        echo "define ('GAME$v1[0]_ANSWER$v1[1]', '$v1[5]');\n";
        fwrite($myfile,"define ('GAME$v1[0]_ANSWER$v1[1]', '$v1[5]');\n");
        echo "define ('GAME$v1[0]_TRIVIA$v1[1]', '$v1[6]');\n";
        fwrite($myfile,"define ('GAME$v1[0]_TRIVIA$v1[1]', '$v1[6]');\n");
        echo "define ('GAME$v1[0]_ANSWERIMAGE$v1[1]', 'images/$v1[7]');\n";
        fwrite($myfile, "define ('GAME$v1[0]_ANSWERIMAGE$v1[1]', 'images/$v1[7]');\n");
        echo "define ('GAME$v1[0]_ANSWERQUOTE$v1[1]', '$v1[8]');\n";
        fwrite($myfile,"define ('GAME$v1[0]_ANSWERQUOTE$v1[1]', '$v1[8]');\n");
        echo "define ('GAME$v1[0]_FORMALANSWER$v1[1]', '$v1[9]');\n";
        fwrite($myfile,"define ('GAME$v1[0]_FORMALANSWER$v1[1]', '$v1[9]');\n");
        echo "define ('GAME$v1[0]_CLUETYPE$v1[1]', '$v1[10]');\n";
        fwrite($myfile,"define ('GAME$v1[0]_CLUETYPE$v1[1]', '$v1[10]');\n");
        echo "define ('GAME$v1[0]_HINT$v1[1]', '$v1[11]');\n";
        fwrite($myfile,"define ('GAME$v1[0]_HINT$v1[1]', '$v1[11]');\n");
        echo "\n\n";
        fwrite($myfile,"\n\n");

}

        fclose($myfile );
    ?>
