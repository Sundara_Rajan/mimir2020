
<?php
// Initialize the session
session_start();

 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login_game.php");
    
    exit;
    
}
?>
<!DOCTYPE html>
<html>
 <head>
  <title>Welcome to Mimir</title>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
  <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="imageDisplay.css" />
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="materialize/css/materialize.min.css"  media="screen,projection"/>

      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  
 
 </head>
 
 
 <body id="top-body">
  


    <!-- Navigation -->
    <nav class="blue" style="min-height:160px" >
    <div class="nav-wrapper">
      <div>
        <a href="#!" class="brand-logo"><img src="images/mimir.gif" width="160"/></a>
      </div>
     
      <ul class="right hide-on-med-and-down">
        <li><a href="sass.html"><i class="material-icons">search</i></a></li>
        <li><a href="badges.html"><i class="material-icons">view_module</i></a></li>
        <li><a href="collapsible.html"><i class="material-icons">refresh</i></a></li>
        <li><a href="mobile.html"><i class="material-icons">more_vert</i></a></li>
      </ul>
    </div>
  </nav>

    <!-- actual navigation tags removed for clarity -->

    <div>
    <blockquote>
    <h2>Mimir</h2>
    Mimir means the rememberer. Beneath the roots of the world tree Yggdrasil there lies a well called Mimirbrunnr, filled with all the wisdom in the world, named after the wise god Mimir who resides there. 
    </blockquote> 

    </div>
    
    <!-- Top Section -->
    <!-- ... -->
    <div class="container">
        <!-- Page Content goes here -->

        <!-- Row 1 -->

        
        <div class="row">
          <!-- game 1 -->
          <div class="col s12 m4">
            <div class="card">
              <div class="card-image">
                <a href="game3.php">
                  <img src="images/missingcup.jpg">
                </a>
                <span class="card-title brown">Whodunnit: Mystery  of  the missing  Cup</span>
              </div>
            
              <div class="card-action">
                <p>You will identify seven famous sports personalities in this exciting game.</p>
              </div>

              <div class="card-action"> 
               <a href="game3.php" >Click me to start the game !!</a>
              </div>
            </div> 
          
          </div><!-- end of game 1 -->

          <!-- game 2 -->
          <div class="col s12 m4">
            <div class="card">
              <div class="card-image">
                <a href="game4.php">
                  <img src="images/recipe.jpg">
                </a>
                <span class="card-title brown">Delicious : Hunt for the golden ingredients </span>
              </div>
            
              <div class="card-action">
                <p>Identify the key ingredients of an interesting dish in this six clue game</p>
              </div>

              <div class="card-action"> 
               <a href="game4.php" >Click me to start the game !!</a>
              </div>
            </div> 
          
          </div><!-- end of game 2 -->
           <!-- game 3 -->
           <div class="col s12 m4">
            <div class="card">
              <div class="card-image">
                <a href="game5.php">
                  <img src="images/ambassador.jpg">
                </a>
                <span class="card-title brown">Delicious : Hunt for the golden ingredients </span>
              </div>
            
              <div class="card-action">
                <p>An ambassador has gone missing and Scotland yard is askimg for your help</p>
              </div>

              <div class="card-action"> 
               <a href="game5.php" >Click me to start the game !!</a>
              </div>
            </div> 
          
          </div><!-- end of game 2 -->
        

    

      </div><!-- End of Row 1 -->



    </div><!-- end of container -->

    <!-- Games Areas -->
    <!-- ... --> 


    
    <!-- Footer -->
    <footer class="blue">
          <div class="container">
            <div class="row">
              
            
            </div>
          </div>
          <div class="footer-copyright">
            <div class="container">
            © 2014 Copyright Text
            <a class="grey-text text-lighten-4 right" href="#!">More Links</a>
            </div>
          </div>
        </footer>
    
    <!-- Bootstrap script imports -->
    <!-- ... -->

  <!--JavaScript at end of body for optimized loading-->
  <script type="text/javascript" src="materialize/js/materialize.min.js"></script>

  


 </body>



</html>


