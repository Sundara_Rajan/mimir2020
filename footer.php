<footer class="footer_section">
		<div class="container">
			<div class="row">
				<div class="col-12 col-sm-6">
					<div class="text-center text-sm-left">
						<p class="footer_text text-white">© 2014 Copyright Text</p>
					</div>
				</div>
				<div class="col-12 col-sm-6">
					<div class="text-center text-sm-right mt-2 mt-sm-0">
						<a href="#" class="footer_text text-white">More Links</a>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<!-- bootstrap -->
	<script src="js/jquery.min.js"></script>
	<script src="js/js.cookie.js"></script>
	<script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/processor.js"></script>
  <script src="js/timer.js"></script>
  
		<!-- aos -->
	<script src="js/aos.js"></script>
	<script type="text/javascript">
		AOS.init();
	</script>
</body>
</html>