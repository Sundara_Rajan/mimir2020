
<?php
// Initialize the session
session_start();

 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login_game.php");
    
    exit;
    
}
?>

<style>

  
</style>

<!DOCTYPE html>
<html>
 <head>
  <title>Welcome to Mimir</title>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
  <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="imageDisplay.css" />
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="materialize/css/materialize.min.css"  media="screen,projection"/>

      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

 </head>
 
 
 <body>
 
  <?php $loggedName = htmlspecialchars($_SESSION["username"]); ?> 
 

  <!-- Navigation -->
    <nav class="blue" style="min-height:160px" >
    <div class="nav-wrapper">
      <a href="welcome_game.php" class="brand-logo"><img src="images/mimir.gif" width="160"/></a>
      <ul class="right hide-on-med-and-down">
        <li><a href="#"><i class="material-icons">search</i></a></li>
        <li><a href="#"><i class="material-icons">view_module</i></a></li>
     
      </ul>
    </div>
  </nav>


  <div class="container">
        <!-- Page Content goes here -->
      <div class="row">
          <div class="col s12 m8">
            <div class="card blue-grey lighten-3">
                
                <div class="card-content black-text">
                  <span class="card-title">Michelin Star Chef game </span>
                </div>
                
                <div class="card-content black-text">
                  <p>While walking down the street, one fine sunny afternoon for your grocery shopping, you see two men in black. You notice them, but think nothing of it. Before you know it you have been abducted and find yourself on a private jet. When you land, you find yourself in the country of Gorgovia. You realise that you have been mistaken for the famous chef Jordon Hamsay and to save your skin you have to cook a fabulous meal for the dictator of Gorgovia. But before you do that you need to identify the following ingredients…. 
</p>
                </div>

            </div>  
          </div>
      

      <div class="col s12 m8">
      <div class="card">
            <div class="card-image">
              <img src="images/Intro-Image.jpg">
              <span class="card-title">Michelin Star Chef</span>
            </div>
          </div>
      </div>

      
      <div class="col s12 m8">
            <div class="card blue-grey lighten-3">
                
                <div class="card-content black-text">
                  <span class="card-title">Details of the game </span>
                </div>
                
                <div class="card-content black-text">
                  <p>This game has six clues that need to be solved.The difficulty rating of the game is medium. The game involves finding out ingredients for a recipe.ø
                </div>

            </div>  
          </div>



      </div>


    

    </div>

    <div class="row">

    <div id="div" class="row center">
    
    <a href="mainProcessor.php?clue=1&game=4" class="waves-effect waves-light btn-large blue">Click me to start the game !!</a>

 

    </div>
    </div>





    <!-- Footer -->
    <footer class="blue">
          <div class="container">
            <div class="row">
              
            
            </div>
          </div>
          <div class="footer-copyright">
            <div class="container">
            © 2020 Copyright Text
            <a class="grey-text text-lighten-4 right" href="#!">More Links</a>
            </div>
          </div>
        </footer>

       

 </body>


 <script>



</script>



</html>
