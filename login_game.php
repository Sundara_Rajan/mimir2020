<?php
// Initialize the session
session_start();
 
// Check if the user is already logged in, if yes then redirect him to welcome page
if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true){
  header("location: index.html");
  exit;
}
 
// Include config file
require_once "config.php";
 
// Define variables and initialize with empty values
$username = $password = "";
$username_err = $password_err = "";
 
// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){
 
    // Check if username is empty
    if(empty(trim($_POST["username"]))){
        $username_err = "Please enter username.";
    } else{
        $username = trim($_POST["username"]);
    }
    
    // Check if password is empty
    if(empty(trim($_POST["password"]))){
        $password_err = "Please enter your password.";
    } else{
        $password = trim($_POST["password"]);
    }
    
    // Validate credentials
    if(empty($username_err) && empty($password_err)){
        // Prepare a select statement
        $sql = "SELECT id, username, password FROM users WHERE username = ?";
        
        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "s", $param_username);
            
            // Set parameters
            $param_username = $username;
            
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                // Store result
                mysqli_stmt_store_result($stmt);
                
                // Check if username exists, if yes then verify password
                if(mysqli_stmt_num_rows($stmt) == 1){                    
                    // Bind result variables
                    mysqli_stmt_bind_result($stmt, $id, $username, $hashed_password);
                    if(mysqli_stmt_fetch($stmt)){
                        if(password_verify($password, $hashed_password)){
                            // Password is correct, so start a new session
                            session_start();
                            
                            // Store data in session variables
                            $_SESSION["loggedin"] = true;
                            $_SESSION["id"] = $id;
                            $_SESSION["username"] = $username;                            
                            
                            // Redirect user to welcome page
                            header("location: index.html");
                        } else{
                            // Display an error message if password is not valid
                            $password_err = "The password you entered was not valid.";
                        }
                    }
                } else{
                    // Display an error message if username doesn't exist
                    $username_err = "No account found with that username.";
                }
            } else{
                echo "Oops! Something went wrong. Please try again later.";
            }

            // Close statement
            mysqli_stmt_close($stmt);
        }
    }
    
    // Close connection
    mysqli_close($link);
}
?>
 
 <!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Welcome to Mimir</title>
	<!-- aos -->
	<link href="css/aos.css" rel="stylesheet">	
	<!-- bootstrap -->
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<!-- style.css -->
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>

	<header>
		<nav class="navbar navbar-expand navbar-light">
			<!-- Brand/logo -->
			<a class="navbar-brand logo" href="index.html">
				<img src="images/logo.gif" alt="logo">
			</a>

			<div class="logo_text">
				<p class="py-2 py-lg-0">Where the brain goes to have FUN</p>
			</div>
			<ul class="py-1 py-lg-0 navbar-nav ml-auto">
				<li class="nav-item">
					<a class="nav-link icon_link login" href="#">
						<img src="images/enter.png" class="d-block d-lg-none"> Login
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link icon_link" href="#">
						<img src="images/Path 7.png">
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link icon_link" href="#">
						<img src="images/noun_menu_2310205.png">
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link icon_link" href="#">
						<img src="images/noun_redo_2840884.png">
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link icon_link" href="#">
						<img src="images/noun_menu_521074.png">
					</a>
				</li>
			</ul>
		</nav>
	</header>
	<!-- about -->
	<section class="about_section">
		<div class="container">
			<div class="about_text" data-aos="fade-up">
				<h5>Mimir</h5>
				<p>Mimir means the rememberer. Beneath the roots of the world tree Yggdrasil there lies a well called Mimirbrunnr, filled with all the wisdom in the world, named after the wise god Mimir who resides there.</p>
			</div>
		</div>
    </section>
    
    <section class="about_section">
		<div class="container">

            <div class="wrapper">
            <?php if (@$_GET['registered'] == 'true')
            echo '<p>You have registered successfully. Now login with the credentials you used</p>' ?>
                <h2>Login</h2>
                <p>Please fill in your credentials to login.</p>
                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                    <div class="form-group <?php echo (!empty($username_err)) ? 'has-error' : ''; ?>">
                        <label>Username</label>
                        <input type="text" name="username" class="form-control" value="<?php echo $username; ?>">
                        <span class="help-block"><?php echo $username_err; ?></span>
                    </div>    
                    <div class="form-group <?php echo (!empty($password_err)) ? 'has-error' : ''; ?>">
                        <label>Password</label>
                        <input type="password" name="password" class="form-control">
                        <span class="help-block"><?php echo $password_err; ?></span>
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary" value="Login">
                    </div>
                    <p>Don't have an account? <a href="register_game.php"><b>Sign up now</b></a>.</p>
                </form>
            </div> 
    </div>
    </section>

    <!-- footer -->
	<footer class="footer_section">
		<div class="container">
			<div class="row">
				<div class="col-12 col-sm-6">
					<div class="text-center text-sm-left">
						<p class="footer_text text-white">© 2014 Copyright Text</p>
					</div>
				</div>
				<div class="col-12 col-sm-6">
					<div class="text-center text-sm-right mt-2 mt-sm-0">
						<a href="#" class="footer_text text-white">More Links</a>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<!-- bootstrap -->
	<script src="js/jquery.min.js"></script>
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<!-- aos -->
	<script src="js/aos.js"></script>
	<script type="text/javascript">
		AOS.init();
	</script>
    
    

</body>
</html>
