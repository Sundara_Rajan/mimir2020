<?php
// Include config file
require_once "config.php";
 
// Define variables and initialize with empty values
$username = $password = $confirm_password = $email ="";
$username_err = $password_err = $confirm_password_err = "";
 
// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){
 
    // Validate username
    if(empty(trim($_POST["username"]))){
        $username_err = "Please enter a username.";
    } else{
        // Prepare a select statement
        $sql = "SELECT id FROM users WHERE username = ?";
        
        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "s", $param_username);
            
            // Set parameters
            $param_username = trim($_POST["username"]);
            
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                /* store result */
                mysqli_stmt_store_result($stmt);
                
                if(mysqli_stmt_num_rows($stmt) == 1){
                    $username_err = "This username is already taken.";
                } else{
                    $username = trim($_POST["username"]);
                }
            } else{
                echo "Oops! Something went wrong. Please try again later.";
            }

            // Close statement
            mysqli_stmt_close($stmt);
        }
    }
    
    // Validate password
    if(empty(trim($_POST["password"]))){
        $password_err = "Please enter a password.";     
    } elseif(strlen(trim($_POST["password"])) < 6){
        $password_err = "Password must have atleast 6 characters.";
    } else{
        $password = trim($_POST["password"]);
    }
    
    // Validate confirm password
    if(empty(trim($_POST["confirm_password"]))){
        $confirm_password_err = "Please confirm password.";     
    } else{
        $confirm_password = trim($_POST["confirm_password"]);
        if(empty($password_err) && ($password != $confirm_password)){
            $confirm_password_err = "Password did not match.";
        }
    }


    // very basic email insertion
    $emailid = trim($_POST["email"]);
    
    // Check input errors before inserting in database
    if(empty($username_err) && empty($password_err) && empty($confirm_password_err)){
        
        // Prepare an insert statement
        $sql = "INSERT INTO users (username, password,emailID) VALUES (?, ?,?)";
         
        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "sss", $param_username, $param_password,$emailid);
            
            // Set parameters
            $param_username = $username;
            $param_password = password_hash($password, PASSWORD_DEFAULT); // Creates a password hash
            
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                // Redirect to login page
                header("location: login_game.php?registered=true");
            } else{
                echo "Something went wrong. Please try again later.";
            }

            // Close statement
            mysqli_stmt_close($stmt);
        }   
    }
    
    // Close connection
    mysqli_close($link);
}
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Welcome to Mimir</title>
	<!-- aos -->
	<link href="css/aos.css" rel="stylesheet">	
	<!-- bootstrap -->
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<!-- style.css -->
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>

	<header>
		<nav class="navbar navbar-expand navbar-light">
			<!-- Brand/logo -->
			<a class="navbar-brand logo" href="index.html">
				<img src="images/logo.gif" alt="logo">
			</a>

			<div class="logo_text">
				<p class="py-2 py-lg-0">Where the brain goes to have FUN</p>
			</div>
			<ul class="py-1 py-lg-0 navbar-nav ml-auto">
				<li class="nav-item">
					<a class="nav-link icon_link login" href="#">
						<img src="images/enter.png" class="d-block d-lg-none"> Login
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link icon_link" href="#">
						<img src="images/Path 7.png">
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link icon_link" href="#">
						<img src="images/noun_menu_2310205.png">
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link icon_link" href="#">
						<img src="images/noun_redo_2840884.png">
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link icon_link" href="#">
						<img src="images/noun_menu_521074.png">
					</a>
				</li>
			</ul>
		</nav>
	</header>
	<!-- about -->
	<section class="about_section">
		<div class="container">
			<div class="about_text" data-aos="fade-up">
				<h5>Mimir</h5>
				<p>Mimir means the rememberer. Beneath the roots of the world tree Yggdrasil there lies a well called Mimirbrunnr, filled with all the wisdom in the world, named after the wise god Mimir who resides there.</p>
			</div>
		</div>
    </section>
    
    <section class="about_section">
		<div class="container">
        <div class="wrapper">
        <h2>Sign Up</h2>
        <p>Please fill this form to create an account.</p>
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
            <div class="form-group <?php echo (!empty($username_err)) ? 'has-error' : ''; ?>">
                <label>Username</label>
                <input type="text" name="username" class="form-control" value="<?php echo $username; ?>">
                <span class="help-block"><?php echo $username_err; ?></span>
            </div>    
            <div class="form-group <?php echo (!empty($password_err)) ? 'has-error' : ''; ?>">
                <label>Password</label>
                <input type="password" name="password" class="form-control" value="<?php echo $password; ?>">
                <span class="help-block"><?php echo $password_err; ?></span>
            </div>
            <div class="form-group <?php echo (!empty($confirm_password_err)) ? 'has-error' : ''; ?>">
                <label>Confirm Password</label>
                <input type="password" name="confirm_password" class="form-control" value="<?php echo $confirm_password; ?>">
                <span class="help-block"><?php echo $confirm_password_err; ?></span>
            </div>

            <div class="form-group ">
                <label>Email Address</label>
                <input type="email" class="form-control" name="email" id="email" aria-describedby="emailHelp" placeholder="Enter email">
                <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
            </div>



            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Submit">
                <input type="reset" class="btn btn-default" value="Reset">
            </div>
            <p>Already have an account? <a href="login_game.php">Login here</a>.</p>
        </form>
    </div>    
    </div>
    </section>

    <!-- footer -->
	<footer class="footer_section">
		<div class="container">
			<div class="row">
				<div class="col-12 col-sm-6">
					<div class="text-center text-sm-left">
						<p class="footer_text text-white">© 2014 Copyright Text</p>
					</div>
				</div>
				<div class="col-12 col-sm-6">
					<div class="text-center text-sm-right mt-2 mt-sm-0">
						<a href="#" class="footer_text text-white">More Links</a>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<!-- bootstrap -->
	<script src="js/jquery.min.js"></script>
	<script src="js/popper.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<!-- aos -->
	<script src="js/aos.js"></script>
	<script type="text/javascript">
		AOS.init();
	</script>
    
    

</body>
</html>