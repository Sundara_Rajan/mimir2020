<?php
add_action( 'wp_enqueue_scripts', 'enqueue_parent_styles' );

function enqueue_parent_styles() {
   wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );
}


/**
 * Exclude products from a particular category on the shop page, her set to hide the "requested" category
 */

function custom_pre_get_posts_query( $q ) {

    $tax_query = (array) $q->get( 'tax_query' );

    $tax_query[] = array(
           'taxonomy' => 'product_cat',
           'field' => 'slug',
           'terms' => array( 'requested' ), // Don't display products in the clothing category on the shop page.
           'operator' => 'NOT IN'
    );


    $q->set( 'tax_query', $tax_query );

}

add_action( 'woocommerce_product_query', 'custom_pre_get_posts_query' );  

add_filter( 'wc_add_to_cart_message_html', 'bbloomer_custom_add_to_cart_message' );
 
function bbloomer_custom_add_to_cart_message() {
$message = 'Handlekurven er nå oppdatert! :)' ;
return $message;
}


// Show hide section all products in main page
add_action('wp_head','main_toggle_all_section');
function main_toggle_all_section(){
	?>
		<style type="text/css">
			.hide_section{display:none;}
			.variable.type-button.swatches{width:100%!important;}
			.variable.type-color.swatches{width:100%!important;}
		</style>
		<script>
		jQuery(function(){
			jQuery('.see_all_apple').click(function(){
				jQuery('#main_apple').toggle();
				jQuery('#main_apple_all').toggle();
				jQuery('html, body').animate({
					'scrollTop' : jQuery("#apple_section").position().top
				});
			});
			
		});
		</script>
	<?php
	
}





?>