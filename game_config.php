<?php

define ('GAMEGameNo_CLUEIMAGEClueNo', 'images/ClueImageName');
define ('GAMEGameNo_CLUETEXTClueNo', 'ClueText');
define ('GAMEGameNo_CLUETITLEClueNo', 'ClueTitle');
define ('GAMEGameNo_ANSWERClueNo', 'Answer');
define ('GAMEGameNo_TRIVIAClueNo', 'trivia');
define ('GAMEGameNo_ANSWERIMAGEClueNo', 'images/AnswerImageName');
define ('GAMEGameNo_ANSWERQUOTEClueNo', 'AnswerQuote');
define ('GAMEGameNo_FORMALANSWERClueNo', 'FormalAnswer');
define ('GAMEGameNo_CLUETYPEClueNo', 'ClueType');
define ('GAMEGameNo_HINTClueNo', 'HintText');


define ('GAME5_CLUEIMAGE0', 'images/G5C0image.jpg');
define ('GAME5_CLUETEXT0', 'So you think you are a sports buff? Our Professor at Mimir challenges you to take this sports quiz. Identify the seven champions in their respective sporting disciplines and win the Mimir Sports Cup!
');
define ('GAME5_CLUETITLE0', 'The Mimir Sports Challenge');
define ('GAME5_ANSWER0', '');
define ('GAME5_TRIVIA0', '');
define ('GAME5_ANSWERIMAGE0', 'images/');
define ('GAME5_ANSWERQUOTE0', '');
define ('GAME5_FORMALANSWER0', '');
define ('GAME5_CLUETYPE0', 'Image');
define ('GAME5_HINT0', '');


define ('GAME5_CLUEIMAGE1', 'images/G5C1image.jpg');
define ('GAME5_CLUETEXT1', 'The grass is green, the sky is blue');
define ('GAME5_CLUETITLE1', 'Identify this champion');
define ('GAME5_ANSWER1', 'RogerFederer;federer;fedex');
define ('GAME5_TRIVIA1', 'Roger Federers favorite music band is AC/DC');
define ('GAME5_ANSWERIMAGE1', 'images/G5A1image.jpg');
define ('GAME5_ANSWERQUOTE1', 'You got it!');
define ('GAME5_FORMALANSWER1', 'Roger Federer');
define ('GAME5_CLUETYPE1', 'Image');
define ('GAME5_HINT1', 'King of grass courts');


define ('GAME5_CLUEIMAGE2', 'images/Game5C2.jpg');
define ('GAME5_CLUETEXT2', '7 but not 007');
define ('GAME5_CLUETITLE2', 'Identify this wizard');
define ('GAME5_ANSWER2', 'CristianoRonaldo;Ronaldo;CR7');
define ('GAME5_TRIVIA2', 'Cristianos father, the late Dinis Aveiro, chose the name Ronaldo for his son as his favourite actor was Ronald Reagan.
');
define ('GAME5_ANSWERIMAGE2', 'images/G5A2image.jpg');
define ('GAME5_ANSWERQUOTE2', 'Well Done!');
define ('GAME5_FORMALANSWER2', 'Cristiano Ronaldo');
define ('GAME5_CLUETYPE2', 'Image');
define ('GAME5_HINT2', 'Try solving this using the Wingdings font');


define ('GAME5_CLUEIMAGE3', 'images/G5C3image.jpg');
define ('GAME5_CLUETEXT3', 'From under 19 to above everyone!');
define ('GAME5_CLUETITLE3', 'Name this livewire sportsperson');
define ('GAME5_ANSWER3', 'viratkohli;kohli');
define ('GAME5_TRIVIA3', 'Virat Kohli has 4 tattoos on his body, including a golden dragon and a samurai warrior on his hand. 
They are supposed to bring him good luck.
');
define ('GAME5_ANSWERIMAGE3', 'images/G5A3image.jpg');
define ('GAME5_ANSWERQUOTE3', 'Awesome');
define ('GAME5_FORMALANSWER3', 'Virat Kohli');
define ('GAME5_CLUETYPE3', 'Image');
define ('GAME5_HINT3', 'Heard of Virushka?');


define ('GAME5_CLUEIMAGE4', 'images/G5C4image.jpg');
define ('GAME5_CLUETEXT4', 'Is he an animal? Is he a forest? He is a champion!');
define ('GAME5_CLUETITLE4', 'What a pro!');
define ('GAME5_ANSWER4', 'TigerWoods;Woods');
define ('GAME5_TRIVIA4', 'The original Tiger was a Vietnamese friend of his fathers from the army. He had saved Tigers fathers life a few times when they served together in the Vietnam War.');
define ('GAME5_ANSWERIMAGE4', 'images/G5A4image.jpg');
define ('GAME5_ANSWERQUOTE4', 'Indeed it is Tiger Woods');
define ('GAME5_FORMALANSWER4', 'Tiger Woods');
define ('GAME5_CLUETYPE4', 'Image');
define ('GAME5_HINT4', 'He has been the number one player in the world for the most consecutive weeks and for the greatest total number of weeks of any golfer in history.');


define ('GAME5_CLUEIMAGE5', 'images/Game5C5.jpg');
define ('GAME5_CLUETEXT5', 'Can you catch up with this man?');
define ('GAME5_CLUETITLE5', 'Name this champion');
define ('GAME5_ANSWER5', 'UsainBolt;Bolt;Usain');
define ('GAME5_TRIVIA5', 'Usain Bolt is a fan of cricket, football and music. In his free time Bolt also likes to play “Black Ops” and “Call of Duty.” He is a video game buff and plays video games every chance he gets.');
define ('GAME5_ANSWERIMAGE5', 'images/G5A5image.jpg');
define ('GAME5_ANSWERQUOTE5', 'Brilliant!');
define ('GAME5_FORMALANSWER5', 'Usain Bolt');
define ('GAME5_CLUETYPE5', 'Image');
define ('GAME5_HINT5', 'Figure out the brand names and then the letters corresponding to the numbers below the brand');


define ('GAME5_CLUEIMAGE6', 'images/G5C6audio.mp3');
define ('GAME5_CLUETEXT6', 'You dont want to mess with him!');
define ('GAME5_CLUETITLE6', 'Name this king of the ring');
define ('GAME5_ANSWER6', 'Mohammadali;muhammadali;mohamudali;ali;cassiusclay');
define ('GAME5_TRIVIA6', 'Ali starred in a Broadway musical.
During his 43-month forced exile from the ring, Ali took to the stage in the title role of the musical Buck White
');
define ('GAME5_ANSWERIMAGE6', 'images/G5A6image.jpg');
define ('GAME5_ANSWERQUOTE6', 'Left uppercut that!');
define ('GAME5_FORMALANSWER6', 'Mohammad Ali');
define ('GAME5_CLUETYPE6', 'Audio');
define ('GAME5_HINT6', 'Float like a butterfly, sting like a bee');


define ('GAME5_CLUEIMAGE7', 'images/G5C7image.jpg');
define ('GAME5_CLUETEXT7', 'Who is this wizard of the feet?');
define ('GAME5_CLUETITLE7', 'Name this young sportsperson');
define ('GAME5_ANSWER7', 'NeymarJunior;Neymar;NeymarJr');
define ('GAME5_TRIVIA7', 'Neymar became a father at the age of 19. He kept the name of the mother hidden.');
define ('GAME5_ANSWERIMAGE7', 'images/G5A7image.jpg');
define ('GAME5_ANSWERQUOTE7', 'Gooooaaaalllll!');
define ('GAME5_FORMALANSWER7', 'Neymar Junior');
define ('GAME5_CLUETYPE7', 'Image');
define ('GAME5_HINT7', 'Samba dancer');


define ('GAME5_CLUEIMAGE8', 'images/G5C8image.jpg');
define ('GAME5_CLUETEXT8', 'WELL DONE ! Enjoy your cup and the coffee!');
define ('GAME5_CLUETITLE8', '');
define ('GAME5_ANSWER8', 'Cupofcoffee');
define ('GAME5_TRIVIA8', '');
define ('GAME5_ANSWERIMAGE8', 'images/');
define ('GAME5_ANSWERQUOTE8', 'Thank you for playing this game. Do log on to MIMIR and check out all our interesting games');
define ('GAME5_FORMALANSWER8', '');
define ('GAME5_CLUETYPE8', 'Image');
define ('GAME5_HINT8', '');


define ('GAME6_CLUEIMAGE0', 'images/G6C0image.jpg');
define ('GAME6_CLUETEXT0', 'Fancy yourself as a movie buff? Why dont you play this game and find out if you are a champion?');
define ('GAME6_CLUETITLE0', 'The Mimir Movie Quiz');
define ('GAME6_ANSWER0', '');
define ('GAME6_TRIVIA0', '');
define ('GAME6_ANSWERIMAGE0', 'images/');
define ('GAME6_ANSWERQUOTE0', '');
define ('GAME6_FORMALANSWER0', '');
define ('GAME6_CLUETYPE0', 'Image');
define ('GAME6_HINT0', '');


define ('GAME6_CLUEIMAGE1', 'images/G6C1audio.mp3');
define ('GAME6_CLUETEXT1', 'Which iconic character does this music track represent?');
define ('GAME6_CLUETITLE1', 'Dapper and Smooth');
define ('GAME6_ANSWER1', 'JamesBond;007;Bond');
define ('GAME6_TRIVIA1', 'Bond was born in Germany, raised in Scotland and England, and considers himself Scottish.');
define ('GAME6_ANSWERIMAGE1', 'images/G6A1image.jpg');
define ('GAME6_ANSWERQUOTE1', 'The name is Bond. James Bond!');
define ('GAME6_FORMALANSWER1', 'James Bond');
define ('GAME6_CLUETYPE1', 'Audio');
define ('GAME6_HINT1', 'He likes it shaken not stirred');


define ('GAME6_CLUEIMAGE2', 'images/G6C2audio.mp3');
define ('GAME6_CLUETEXT2', 'Which movie franchise has this signature music track?');
define ('GAME6_CLUETITLE2', 'Hunt down this franchise');
define ('GAME6_ANSWER2', 'MissionImpossible;MI');
define ('GAME6_TRIVIA2', 'In the MI franchise, the organisation IMF stands for Impossible Mission Force');
define ('GAME6_ANSWERIMAGE2', 'images/G6A2image.jpg');
define ('GAME6_ANSWERQUOTE2', 'Thats right!');
define ('GAME6_FORMALANSWER2', 'Mission Impossible');
define ('GAME6_CLUETYPE2', 'Audio');
define ('GAME6_HINT2', 'It is a series of American action spy films both based on and a follow-on from the television series of the same name created by Bruce Geller');


define ('GAME6_CLUEIMAGE3', 'images/G6C3image.jpg');
define ('GAME6_CLUETEXT3', 'Name the Director of this movie trilogy that redefined Amercian pop culture');
define ('GAME6_CLUETITLE3', 'Revenge is a dish best served cold!');
define ('GAME6_ANSWER3', 'FrancisFordCoppola;Coppola;FrancisCoppola;Francis');
define ('GAME6_TRIVIA3', 'Some of the original music scores of The Godfather trilogy were contributed by Francis Ford Coppolas own father, Carmine Coppola');
define ('GAME6_ANSWERIMAGE3', 'images/G6A3image.jpg');
define ('GAME6_ANSWERQUOTE3', 'You got it!!');
define ('GAME6_FORMALANSWER3', 'Francis Ford Coppola');
define ('GAME6_CLUETYPE3', 'Image');
define ('GAME6_HINT3', 'The series chronicles the Mafia empire set up by Italian immigrant Vito Corleone and his family');


define ('GAME6_CLUEIMAGE4', 'images/G6C4video.mp4');
define ('GAME6_CLUETEXT4', 'Name this Iconic Film');
define ('GAME6_CLUETITLE4', '1939 Amercian Classic');
define ('GAME6_ANSWER4', 'GonewiththeWind');
define ('GAME6_TRIVIA4', 'Clark Gable would sometimes eat garlic before his kissing scenes with Vivian Leigh as a prank');
define ('GAME6_ANSWERIMAGE4', 'images/G6A4image.jpg');
define ('GAME6_ANSWERQUOTE4', 'Perfect!');
define ('GAME6_FORMALANSWER4', 'Gone With The Wind');
define ('GAME6_CLUETYPE4', 'Video');
define ('GAME6_HINT4', 'It is adapted from the novel by Margaret Mitchell');


define ('GAME6_CLUEIMAGE5', 'images/G6C5image.jpg');
define ('GAME6_CLUETEXT5', 'Name this Ace Director');
define ('GAME6_CLUETITLE5', 'Iron Jim Jim');
define ('GAME6_ANSWER5', 'JamesCameron;Cameron;James;JimCameron');
define ('GAME6_TRIVIA5', 'James Camerons first film, a 10-minute short called Xenogenesis, was made with $20,000 raised from local dentists');
define ('GAME6_ANSWERIMAGE5', 'images/G6A5image.jpg');
define ('GAME6_ANSWERQUOTE5', 'Brilliant Answer');
define ('GAME6_FORMALANSWER5', 'James Cameron');
define ('GAME6_CLUETYPE5', 'Image');
define ('GAME6_HINT5', 'He is a Canadian film director, producer, screenwriter, and environmentalist.');


define ('GAME6_CLUEIMAGE6', 'images/G6C6video.mp4');
define ('GAME6_CLUETEXT6', 'Name this cult film');
define ('GAME6_CLUETITLE6', 'Jules, Vincent and Butch');
define ('GAME6_ANSWER6', 'PulpFiction');
define ('GAME6_TRIVIA6', 'This was one of the first movies to use the Internet for advertising.');
define ('GAME6_ANSWERIMAGE6', 'images/G6A6image.jpg');
define ('GAME6_ANSWERQUOTE6', 'Super Show. Correct Answer! Thank you for playing this game. Do log on to MIMIR and check out all our interesting games');
define ('GAME6_FORMALANSWER6', 'Pulp Fiction');
define ('GAME6_CLUETYPE6', 'Video');
define ('GAME6_HINT6', 'It is a 1994 American neo-noir black comedy crime film written and directed by Quentin Tarantino');


define ('GAME1_CLUEIMAGE0', 'images/G1C0Image.jpg');
define ('GAME1_CLUETEXT0', 'An ambassador from the country of Gernobia has gone missing and Scotland Yard is asking for your help');
define ('GAME1_CLUETITLE0', 'Mystery of the Missing Ambassador');
define ('GAME1_ANSWER0', '');
define ('GAME1_TRIVIA0', '');
define ('GAME1_ANSWERIMAGE0', 'images/');
define ('GAME1_ANSWERQUOTE0', '');
define ('GAME1_FORMALANSWER0', '');
define ('GAME1_CLUETYPE0', '');
define ('GAME1_HINT0', '');


define ('GAME1_CLUEIMAGE1', 'images/G1C1image.jpg');
define ('GAME1_CLUETEXT1', 'You have been invited to the Scotland Yard to help them in tracking the missing Gernobian Ambassador. After surveying you little doubtfully, they hand over a chit bearing an address');
define ('GAME1_CLUETITLE1', 'Welcome to Scotland Yard');
define ('GAME1_ANSWER1', 'WestminsterAbbey;WestministerAbbey;Westminster;Westminister');
define ('GAME1_TRIVIA1', 'Westminster was originally an island inhabited by Monks');
define ('GAME1_ANSWERIMAGE1', 'images/G1A1image.jpg');
define ('GAME1_ANSWERQUOTE1', 'Excellent!');
define ('GAME1_FORMALANSWER1', 'Westminster Abbey');
define ('GAME1_CLUETYPE1', 'Image');
define ('GAME1_HINT1', 'It is one of United Kingdoms most notable religious buildings and the traditional place of coronation and burial for English and, later, British monarchs');


define ('GAME1_CLUEIMAGE2', 'images/G1C2image.jpg');
define ('GAME1_CLUETEXT2', 'This is the famous underground stop of Westminister. Hmm... So the Ambassador was here. What was he doing here? The ticket seller remembers the Ambassador well. He was amusing himself with a set of numbers. "This will tell you where to go," he said.');
define ('GAME1_CLUETITLE2', 'Where did he go next');
define ('GAME1_ANSWER2', 'toweroflondon;londontower');
define ('GAME1_TRIVIA2', 'There are six ravens in the Tower of London. Legend says that if  they leave, Britain will fall. ');
define ('GAME1_ANSWERIMAGE2', 'images/G1A2image.jpg');
define ('GAME1_ANSWERQUOTE2', 'Bravo!');
define ('GAME1_FORMALANSWER2', 'Tower of London');
define ('GAME1_CLUETYPE2', 'Image');
define ('GAME1_HINT2', 'These are map co-ordinates of a famous place in London :)');


define ('GAME1_CLUEIMAGE3', 'images/G1C3image.jpg');
define ('GAME1_CLUETEXT3', 'You reached the Tower of London and ask the Beefeaters if they have any leads on the Ambassador. First, they wouldnt talk. You know how they are. But one of them slipped you a piece of paper and moved his eyebrows mysteriously.');
define ('GAME1_CLUETITLE3', 'Did the Ambassador go pickle shopping ?');
define ('GAME1_ANSWER3', 'Gherkin');
define ('GAME1_TRIVIA3', 'Despite its curved shape, the building uses only one piece of curved glass – the lens right at the top.');
define ('GAME1_ANSWERIMAGE3', 'images/G1A3image.jpg');
define ('GAME1_ANSWERQUOTE3', 'That was not too difficult, was it?');
define ('GAME1_FORMALANSWER3', 'Gherkin');
define ('GAME1_CLUETYPE3', 'Image');
define ('GAME1_HINT3', 'Put the capital letters in their right place - you get the name of a street. Figure out which famous commercial building is on this street');


define ('GAME1_CLUEIMAGE4', 'images/G1Clue4.m4a');
define ('GAME1_CLUETEXT4', 'You reach the Gherkin and walk into its swanky reception. After inquiring about the Gernobian Ambassador with a snooty receptionist who made the Beefeater look friendly, you are provided with an audio clip.');
define ('GAME1_CLUETITLE4', 'Pardon me what did you say please');
define ('GAME1_ANSWER4', '22BBakerStreet;221B;221BakerStreet');
define ('GAME1_TRIVIA4', 'The true address is 239 Baker Street which now houses a museum');
define ('GAME1_ANSWERIMAGE4', 'images/G1A4image.jpg');
define ('GAME1_ANSWERQUOTE4', 'Excellent!');
define ('GAME1_FORMALANSWER4', '221 B Baker Street');
define ('GAME1_CLUETYPE4', 'Audio');
define ('GAME1_HINT4', 'You will need a reverse audio player to figure out what is being said and then find out an address');


define ('GAME1_CLUEIMAGE5', 'images/G1C5image.jpg');
define ('GAME1_CLUETEXT5', 'You knock on the door of 221B a couple of times. An elderly gentleman with a slight limp in his right leg opens the door and welcomes you in. The sweet smell of opium wafts through the hall. A tall lean man walks in and stares at you with a piercing look. “Are you looking for the Ambassador?” he asks. He can see that you are startled and he mutters under his breath “You need to pay a visit to a powerful man. Some people call him Bob the Builder". Whats the address of his residence?');
define ('GAME1_CLUETITLE5', 'Yes Prime Minister');
define ('GAME1_ANSWER5', '10DowningStreet;downingstreet;No10Downingstreet');
define ('GAME1_TRIVIA5', 'Downing Street has a resident cat called Larry with the title "Chief Mouser"');
define ('GAME1_ANSWERIMAGE5', 'images/G1A5image.jpg');
define ('GAME1_ANSWERQUOTE5', 'Brilliant!');
define ('GAME1_FORMALANSWER5', '10 Downing Street');
define ('GAME1_CLUETYPE5', 'Image');
define ('GAME1_HINT5', 'It is the official residence of the Prime Minister of the United Kingdom');


define ('GAME1_CLUEIMAGE6', 'images/G1C6image.jpg');
define ('GAME1_CLUETEXT6', '10, Downing Street. Finally a good cup of tea and the hope that Gernobian noble is actually here. The Prime Minister grimaces and takes back the cup of tea you had eagerly reached for when you mention the Gernobian Ambassador. He scowls and says, "yes, he is here but guess what he is upto?"');
define ('GAME1_CLUETITLE6', 'Really ?? What was he busy doing?');
define ('GAME1_ANSWER6', 'HavingLunch;Lunch;LunchwithBoris;Dining;Eating;Lunching');
define ('GAME1_TRIVIA6', 'In some countries, lunch is the most important meal of the day. The abbreviation “lunch” is taken from the more formal Northern English word “luncheon”, which is derived from the Anglo-Saxon word nuncheon or nunchin meaning “noon drink.” ');
define ('GAME1_ANSWERIMAGE6', 'images/G1A6image.jpg');
define ('GAME1_ANSWERQUOTE6', 'Yes he was having lunch with the Prime Minister');
define ('GAME1_FORMALANSWER6', 'Having Lunch');
define ('GAME1_CLUETYPE6', 'Image');
define ('GAME1_HINT6', 'A gastronomic activity usually undertaken in the afternoon');


define ('GAME3_CLUEIMAGE0', 'images/G3C0image.jpg');
define ('GAME3_CLUETEXT0', 'Are you a story teller? Here is your chance to make a story while solving movie mimirs');
define ('GAME3_CLUETITLE0', 'Build Your Story!');
define ('GAME3_ANSWER0', '');
define ('GAME3_TRIVIA0', '');
define ('GAME3_ANSWERIMAGE0', 'images/');
define ('GAME3_ANSWERQUOTE0', '');
define ('GAME3_FORMALANSWER0', '');
define ('GAME3_CLUETYPE0', 'Image');
define ('GAME3_HINT0', '');


define ('GAME3_CLUEIMAGE1', 'images/G3C1image.jpg');
define ('GAME3_CLUETEXT1', 'Lets start by paying a tribute to these three gentlemen. What say? We are not looking for who they are or their names, but what they did. What they did is key to the subsequent clues ;)');
define ('GAME3_CLUETITLE1', 'Who are these gents?');
define ('GAME3_ANSWER1', 'youtubefounders;youtube;foundersofyoutube;inventedyoutube;youtubeinventors;foundyoutube');
define ('GAME3_TRIVIA1', 'YouTube was founded by Steve Chen, Chad Hurley and Jawed Karim who were all early employees of PayPal.');
define ('GAME3_ANSWERIMAGE1', 'images/G3A1image.jpg');
define ('GAME3_ANSWERQUOTE1', 'Brilliant!');
define ('GAME3_FORMALANSWER1', 'YouTube Founders');
define ('GAME3_CLUETYPE1', 'Image');
define ('GAME3_HINT1', 'They invented a video-sharing website where users can upload, share and view content');


define ('GAME3_CLUEIMAGE2', 'images/G3C2text.jpg');
define ('GAME3_CLUETEXT2', 'Let us begin the story by filling in the missing word! I think I just miss my _ _ _ _ _ _. Where has he gone?');
define ('GAME3_CLUETITLE2', 'Identify the missing word');
define ('GAME3_ANSWER2', 'friend');
define ('GAME3_TRIVIA2', 'Andy and Reds opening chat in the prison yard, in which Red is throwing a baseball, took nine hours to shoot. Morgan Freeman threw the baseball for the entire nine hours without a word of complaint. He showed up for work the next day with his left arm in a sling');
define ('GAME3_ANSWERIMAGE2', 'images/G3A2image.jpg');
define ('GAME3_ANSWERQUOTE2', 'Excellent!');
define ('GAME3_FORMALANSWER2', 'Friend');
define ('GAME3_CLUETYPE2', 'Image');
define ('GAME3_HINT2', 'The code above points to a specific video and the numbers tell the exact time at which to look for the word');


define ('GAME3_CLUEIMAGE3', 'images/G3C3text.jpg');
define ('GAME3_CLUETEXT3', 'I would like to wish him _ _ _ _    _ _ _ _ ');
define ('GAME3_CLUETITLE3', 'All the Best!');
define ('GAME3_ANSWER3', 'goodluck');
define ('GAME3_TRIVIA3', 'Jeff Bridges was first cast as Bryan Mills but after he dropped out of the project, Liam Neeson accepted the part, desiring to play a more physically demanding role than he was used to. Bridges eventually saw the film and said Neeson was a much better choice for the role.');
define ('GAME3_ANSWERIMAGE3', 'images/G3A3image.jpg');
define ('GAME3_ANSWERQUOTE3', 'Nice!');
define ('GAME3_FORMALANSWER3', 'Good Luck');
define ('GAME3_CLUETYPE3', 'Image');
define ('GAME3_HINT3', 'The code above points to a specific video and the numbers tell the exact time at which to look for the word');


define ('GAME3_CLUEIMAGE4', 'images/G3C4text.jpg');
define ('GAME3_CLUETEXT4', 'I went to his favourite café. His sister was singing a song about his pet _ _ _ _ _ _    _ _ _');
define ('GAME3_CLUETITLE4', 'Friend in need is a friend indeed');
define ('GAME3_ANSWER4', 'smellycat');
define ('GAME3_TRIVIA4', 'At a 2015 Taylor Swift concert, Lisa Kudrow sang "Smelly Cat" live.');
define ('GAME3_ANSWERIMAGE4', 'images/G3A4image.jpg');
define ('GAME3_ANSWERQUOTE4', 'Oh yeah!');
define ('GAME3_FORMALANSWER4', 'SmellyCat');
define ('GAME3_CLUETYPE4', 'Image');
define ('GAME3_HINT4', 'The code above points to a specific video. What is the video about?');


define ('GAME3_CLUEIMAGE5', 'images/G3C5text.jpg');
define ('GAME3_CLUETEXT5', 'His sister told me that he has found                 _ _ _ _ _ _ _ _ in _ _ _ _ _ _ _ ');
define ('GAME3_CLUETITLE5', 'Godfathers');
define ('GAME3_ANSWER5', 'paradiseinamerica;paradiseamerica');
define ('GAME3_TRIVIA5', 'When Coppola initially mentioned Brando as a possibility for Vito Corleone, the head of Paramount, Charles Bluhdorn, told Coppola the actor would “never appear in a Paramount picture.”');
define ('GAME3_ANSWERIMAGE5', 'images/G3A5image.jpg');
define ('GAME3_ANSWERQUOTE5', 'Good Show. It is the dialogue from The Godfather');
define ('GAME3_FORMALANSWER5', 'Paradise in America');
define ('GAME3_CLUETYPE5', 'Image');
define ('GAME3_HINT5', 'The code above points to a specific video and the numbers tell the exact time at which to look for the words');


define ('GAME3_CLUEIMAGE6', 'images/G3C6text.jpg');
define ('GAME3_CLUETEXT6', 'Asking my friend to return from America is like _ _ _ _ _ _ _      _ _ _ _ _ _ _ _ _ _ ');
define ('GAME3_CLUETITLE6', 'Tom Cruise');
define ('GAME3_ANSWER6', 'missionimpossible;mi');
define ('GAME3_TRIVIA6', 'The producers cast Ving Rhames as Luther because they felt he was the opposite of what a hacker normally looks like.');
define ('GAME3_ANSWERIMAGE6', 'images/Game3A6.jpg');
define ('GAME3_ANSWERQUOTE6', 'Great Work. You are almost there!');
define ('GAME3_FORMALANSWER6', 'Mission Impossible');
define ('GAME3_CLUETYPE6', 'Image');
define ('GAME3_HINT6', 'The code above points to a specific video and the soundtrack of that video should given you the clue to the answer');


define ('GAME3_CLUEIMAGE7', 'images/G3C7text.jpg');
define ('GAME3_CLUETEXT7', 'My friend says he will stay and “Make _ _ _ _ _ _ _     _ _ _ _ _      _ _ _ _ _ ”');
define ('GAME3_CLUETITLE7', 'POTUS');
define ('GAME3_ANSWER7', 'Americagreatagain;MakeAmericaGreatAgain');
define ('GAME3_TRIVIA7', 'Trump owned the Miss USA beauty pageant from 1996 to 2015');
define ('GAME3_ANSWERIMAGE7', 'images/G3A7image.jpg');
define ('GAME3_ANSWERQUOTE7', 'Perfecto!  Thank you for playing this game. Do log on to MIMIR and check out all our interesting games');
define ('GAME3_FORMALANSWER7', 'America great again');
define ('GAME3_CLUETYPE7', 'Image');
define ('GAME3_HINT7', 'The code above points to a specific video and the numbers tell the exact time at which to look for the words');


define ('GAME2_CLUEIMAGE0', 'images/G2C0.jpg');
define ('GAME2_CLUETEXT0', 'Welcome to Oslo! This is going to be an exciting journey around the capital city of Norway. We’ll take you to some interesting places and some really cool stories!');
define ('GAME2_CLUETITLE0', 'Velkommen til Oslo!');
define ('GAME2_ANSWER0', '');
define ('GAME2_TRIVIA0', '');
define ('GAME2_ANSWERIMAGE0', 'images/');
define ('GAME2_ANSWERQUOTE0', '');
define ('GAME2_FORMALANSWER0', '');
define ('GAME2_CLUETYPE0', '');
define ('GAME2_HINT0', '');


define ('GAME2_CLUEIMAGE1', 'images/G2C1Image.jpg');
define ('GAME2_CLUETEXT1', 'You are handed a card that has these two symbols and the number 31 on it. Where in Oslo do you think you would end up at?
');
define ('GAME2_CLUETITLE1', 'Ruter');
define ('GAME2_ANSWER1', 'Fornebu');
define ('GAME2_TRIVIA1', 'Oslo Airport, Fornebu (FBU) served as the main airport for Oslo and the country since before World War II and until the evening of October 7, 1998, when it was closed down. Overnight, a grand moving operation was performed, so that the following morning, the new main airport, located inland at Gardermoen (OSL), opened for operations as the main airport, as opposed to previously having been a minor airport.
');
define ('GAME2_ANSWERIMAGE1', 'images/G2C1A.jpg');
define ('GAME2_ANSWERQUOTE1', 'Way to Go!
');
define ('GAME2_FORMALANSWER1', 'Fornebu');
define ('GAME2_CLUETYPE1', 'Image');
define ('GAME2_HINT1', 'Headquarters of Norways Telecom giant');


define ('GAME2_CLUEIMAGE2', 'images/G2C2Image.jpg');
define ('GAME2_CLUETEXT2', 'What do you think is the next stop based on the symbol and the following text?<br><h3><b>Nbkpstuvfo</b></h3>
');
define ('GAME2_CLUETITLE2', 'T-Bane');
define ('GAME2_ANSWER2', 'Majorstua;Majorstuen');
define ('GAME2_TRIVIA2', 'The place is named after Major Michael Wilhelm Sundt (1729–1759). If it was named after his father (also called Michael Wilhelm Sundt (1679–1753)), it would have been called Generalstuen!!
');
define ('GAME2_ANSWERIMAGE2', 'images/G2C2A.jpg');
define ('GAME2_ANSWERQUOTE2', 'You are on the right track!');
define ('GAME2_FORMALANSWER2', 'Majorstuen');
define ('GAME2_CLUETYPE2', 'Image');
define ('GAME2_HINT2', 'The clue is a cipher. For eg M is represented by N and O by P ( offset by a letter). Now thats just an example.');


define ('GAME2_CLUEIMAGE3', 'images/G2C3Image.jpg');
define ('GAME2_CLUETEXT3', 'What place is the Big O? No, it most definitely is not Oslo!
');
define ('GAME2_CLUETITLE3', 'Big O');
define ('GAME2_ANSWER3', 'Storo');
define ('GAME2_TRIVIA3', 'The neighbourhood is named after the farm Storo. This farm was the big (store) part of the old farm O. The farm was divided into two parts around 1550 AD: Storo ("Store O") and Lillo ("Lille O" - lille means "little").
');
define ('GAME2_ANSWERIMAGE3', 'images/G2C3A.jpg');
define ('GAME2_ANSWERQUOTE3', 'It is Storo!');
define ('GAME2_FORMALANSWER3', 'Storo');
define ('GAME2_CLUETYPE3', 'Image');
define ('GAME2_HINT3', 'Play of words - try translating to Norwegian to form the answer. Also a tram stop.');


define ('GAME2_CLUEIMAGE4', 'images/G2C4Image.jpg');
define ('GAME2_CLUETEXT4', 'You are now given a map. What place are we talking about?
');
define ('GAME2_CLUETITLE4', 'Map');
define ('GAME2_ANSWER4', 'Grønland;gronland');
define ('GAME2_TRIVIA4', 'The neighborhood, along with neighboring Tøyen, is considered to be one of the most ethnically diverse areas in Scandinavia, and is home to a large population of immigrants from Pakistan, Somalia, Turkey and Kurdistan.');
define ('GAME2_ANSWERIMAGE4', 'images/G2C4A.jpg');
define ('GAME2_ANSWERQUOTE4', 'Welcome to Grønland! ');
define ('GAME2_FORMALANSWER4', 'Grønland');
define ('GAME2_CLUETYPE4', 'Image');
define ('GAME2_HINT4', 'The place where south asians go for their grocery shopping');


define ('GAME2_CLUEIMAGE5', 'images/G2C5Image.jpg');
define ('GAME2_CLUETEXT5', 'What place are we now talking about?');
define ('GAME2_CLUETITLE5', 'Morse');
define ('GAME2_ANSWER5', 'Bislett');
define ('GAME2_TRIVIA5', 'It is internationally famous for the Bislett Games, held at Bislett Stadium.
Bislett has also become known to non-Norwegians due to being the home of the fictional detective Harry Hole (created by Jo Nesbø), whose exploits have been translated into numerous languages.
');
define ('GAME2_ANSWERIMAGE5', 'images/G2C5A.jpg');
define ('GAME2_ANSWERQUOTE5', 'Indeed! Thank you for playing this game. Do log on to MIMIR and check out all our interesting games');
define ('GAME2_FORMALANSWER5', 'Bislett');
define ('GAME2_CLUETYPE5', 'Image');
define ('GAME2_HINT5', 'Ever heard of Morse code?');


define ('GAME_CLUEIMAGE', 'images/');
define ('GAME_CLUETEXT', '');
define ('GAME_CLUETITLE', '');
define ('GAME_ANSWER', '');
define ('GAME_TRIVIA', '');
define ('GAME_ANSWERIMAGE', 'images/');
define ('GAME_ANSWERQUOTE', '');
define ('GAME_FORMALANSWER', '');
define ('GAME_CLUETYPE', '');
define ('GAME_HINT', '');


define ('GAME4_CLUEIMAGE0', 'images/recipe.jpg');
define ('GAME4_CLUETEXT0', 'While walking down the street, one fine sunny afternoon for your grocery shopping, you see two men in black. You notice them, but think nothing of it. Before you know it you have been abducted and find yourself on a private jet. When you land, you find yourself in the country of Gorgovia. You realise that you have been mistaken for the famous chef Jordon Hamsay and to save your skin you have to cook a fabulous meal for the dictator of Gorgovia. But before you do that you need to identify the following ingredients');
define ('GAME4_CLUETITLE0', 'Michelin Star Chef Game');
define ('GAME4_ANSWER0', '');
define ('GAME4_TRIVIA0', '');
define ('GAME4_ANSWERIMAGE0', 'images/');
define ('GAME4_ANSWERQUOTE0', '');
define ('GAME4_FORMALANSWER0', '');
define ('GAME4_CLUETYPE0', '');
define ('GAME4_HINT0', '');


define ('GAME4_CLUEIMAGE1', 'images/Clue1-Video.mp4');
define ('GAME4_CLUETEXT1', 'Identify the ingredient that is being manufactured in the video');
define ('GAME4_CLUETITLE1', 'Whats cooking');
define ('GAME4_ANSWER1', 'tofu;soyabeancurd;soybeancurd');
define ('GAME4_TRIVIA1', 'The first written record of tofu dates back 950 AD. One fascinating theory about how it was first produced is that a cook seasoned warmed soy milk with some sea salt. After forgetting it for a long time, the milk began to curdle which produced the curds we now know as tofu!');
define ('GAME4_ANSWERIMAGE1', 'images/Clue1-Image-2.jpg');
define ('GAME4_ANSWERQUOTE1', 'The name Tofu is a Japanese term that is derived from the Chinese†word doufu (Mandarin) that literally means curdled bean');
define ('GAME4_FORMALANSWER1', 'Tofu');
define ('GAME4_CLUETYPE1', 'Video');
define ('GAME4_HINT1', 'No its not cheese. This is a vegetarian alternative to cheese and is made by coagulating soy milk');


define ('GAME4_CLUEIMAGE2', 'images/Clue2-Image-1.jpg');
define ('GAME4_CLUETEXT2', 'Now for some word play. Identify the ingredient from the two pictures');
define ('GAME4_CLUETITLE2', 'Lets get down to the root of it');
define ('GAME4_ANSWER2', 'Potato;Potatoe');
define ('GAME4_TRIVIA2', 'Despite its appearance, potato is made up of 80% water');
define ('GAME4_ANSWERIMAGE2', 'images/Clue2-Image-2.jpg');
define ('GAME4_ANSWERQUOTE2', 'The roots of the humble potato trace back to Peru where the Incas were the first to grow the crop');
define ('GAME4_FORMALANSWER2', 'Potato');
define ('GAME4_CLUETYPE2', 'Image');
define ('GAME4_HINT2', 'Identify the leaf and the slang for it. Now combine it with the other image and you get the answer!');


define ('GAME4_CLUEIMAGE3', 'images/Clue3-Image-1.jpg');
define ('GAME4_CLUETEXT3', 'Look carefully at this famous painting by Monet. What ingredient might we find on these trees?');
define ('GAME4_CLUETITLE3', 'Martini anyone?');
define ('GAME4_ANSWER3', 'Olive;GreenOlive;BlackOlive;Olives');
define ('GAME4_TRIVIA3', 'The average life of an olive tree is between 300 and 600 years. One of the oldest olive trees in the world is on the island of Crete, in Greece. This tree is about 4,000 years old and is still producing olives!');
define ('GAME4_ANSWERIMAGE3', 'images/Clue3-Image-2.jpg');
define ('GAME4_ANSWERQUOTE3', 'Olive it is!! The painting is called Olive Tree Wood in the Moreno Garden.');
define ('GAME4_FORMALANSWER3', 'Olives');
define ('GAME4_CLUETYPE3', 'Image');
define ('GAME4_HINT3', 'Identify the picture with Google Lens');


define ('GAME4_CLUEIMAGE4', 'images/G4C4image.jpg');
define ('GAME4_CLUETEXT4', 'Here is a super food that you need to find out');
define ('GAME4_CLUETITLE4', 'SuperFood');
define ('GAME4_ANSWER4', 'Kale;Kales');
define ('GAME4_TRIVIA4', 'Kale is not particularly tasty. But after frosting it, it becomes sweeter. Perhaps you could try it then!');
define ('GAME4_ANSWERIMAGE4', 'images/G4A4image.jpg');
define ('GAME4_ANSWERQUOTE4', 'Yes Kale indeed. Kale, or leaf cabbage, belongs to a group of cabbage cultivars grown for their edible leaves, although some are used as ornamentals');
define ('GAME4_FORMALANSWER4', 'Kale');
define ('GAME4_CLUETYPE4', 'Image');
define ('GAME4_HINT4', 'Identify each picture and pick the letter corresponding to the number indicated beside it');


define ('GAME4_CLUEIMAGE5', 'images/Clue5-Image-1.jpg');
define ('GAME4_CLUETEXT5', 'The next ingredient consists of two words. The first word is represented by the picture on the left and the  second word is the answer to the riddle on the right');
define ('GAME4_CLUETITLE5', 'Dont cry now. Its easy!');
define ('GAME4_ANSWER5', 'RedOnion;RedOnions;Onion;Onions');
define ('GAME4_TRIVIA5', 'In the Middle Ages, onions were an acceptable form of currency, and were used to pay for rent, goods and services, and even as gifts! Ancient Egyptians worshipped onions, believing their spherical shape and concentric circles within symbolized eternity');
define ('GAME4_ANSWERIMAGE5', 'images/Clue5-Image-2.jpg');
define ('GAME4_ANSWERQUOTE5', 'Wow! You know your movies too!');
define ('GAME4_FORMALANSWER5', 'Red Onions');
define ('GAME4_CLUETYPE5', 'Image');
define ('GAME4_HINT5', 'Remember Shawshank Redemption? Whats the characters name. Now solve the riddle');


define ('GAME4_CLUEIMAGE6', 'images/Clue6-Image-1.jpg');
define ('GAME4_CLUETEXT6', 'And on to the last ingredient. This should be a walk in the park for you. What ingredient could this symbol potentially represent?');
define ('GAME4_CLUETITLE6', 'Pods?');
define ('GAME4_ANSWER6', 'GreenPeas;GreenPea;GardenPeas;GardenPea;Peas;Pea');
define ('GAME4_TRIVIA6', 'That was the beginning of the Green Peas Movement! In the mid-1800s, peas in a monastery garden in Austria were famously used by the monk Gregor Mendel in his pioneering studies of the nature of heredity.');
define ('GAME4_ANSWERIMAGE6', 'images/Clue6-Image-2.jpg');
define ('GAME4_ANSWERQUOTE6', 'Since you managed to identify all the ingredients correctly, you just signed your own release forms! Congratulations! Next time watch your back! Peas out!!');
define ('GAME4_FORMALANSWER6', 'Green Peas');
define ('GAME4_CLUETYPE6', 'Image');
define ('GAME4_HINT6', 'Idenitfy the logo and think of a vegetable');


define ('GAME0_CLUEIMAGE25-11-2020', 'images/G6C6video.mp4');
define ('GAME0_CLUETEXT25-11-2020', 'Name this cult film');
define ('GAME0_CLUETITLE25-11-2020', 'Jules, Vincent and Butch');
define ('GAME0_ANSWER25-11-2020', 'PulpFiction');
define ('GAME0_TRIVIA25-11-2020', 'This was one of the first movies to use the Internet for advertising.');
define ('GAME0_ANSWERIMAGE25-11-2020', 'images/G6A6image.jpg');
define ('GAME0_ANSWERQUOTE25-11-2020', 'Super Show. Correct Answer! Thank you for playing this game. Do log on to MIMIR and check out all our interesting games');
define ('GAME0_FORMALANSWER25-11-2020', 'Pulp Fiction');
define ('GAME0_CLUETYPE25-11-2020', 'Video');
define ('GAME0_HINT25-11-2020', 'It is a 1994 American neo-noir black comedy crime film written and directed by Quentin Tarantino');


define ('GAME0_CLUEIMAGE07-12-2020', 'images/D0-DM.jpg');
define ('GAME0_CLUETEXT07-12-2020', 'Identify the character from the Harry Potter series from the following anagram');
define ('GAME0_CLUETITLE07-12-2020', 'Draco Malfoy');
define ('GAME0_ANSWER07-12-2020', 'Draco Malfoy');
define ('GAME0_TRIVIA07-12-2020', 'Tom Felton, who plays Draco in the film versions of Harry Potter, wasn’t allowed to tan while playing the pasty Malfoy.');
define ('GAME0_ANSWERIMAGE07-12-2020', 'images/Draco.jpg');
define ('GAME0_ANSWERQUOTE07-12-2020', 'Perfect!');
define ('GAME0_FORMALANSWER07-12-2020', 'Draco Malfoy');
define ('GAME0_CLUETYPE07-12-2020', 'Image');
define ('GAME0_HINT07-12-2020', 'He is from Slytherin house');


define ('GAME0_CLUEIMAGE02-12-2020', 'images/C-MS.jpg');
define ('GAME0_CLUETEXT03-12-2020', 'Unscramble the famous brand from this anagram');
define ('GAME0_CLUETITLE03-12-2020', 'Microsoft');
define ('GAME0_ANSWER03-12-2020', 'Microsoft');
define ('GAME0_TRIVIA03-12-2020', 'The rolling hills of the default Windows XP wallpaper is located in Sonoma County, CA and always became a lush green color for one week in early spring. The year after the photo was made, it became a vineyard.');
define ('GAME0_ANSWERIMAGE03-12-2020', 'images/mslogo.jpg');
define ('GAME0_ANSWERQUOTE03-12-2020', 'Bingo!');
define ('GAME0_FORMALANSWER03-12-2020', 'Microsoft');
define ('GAME0_CLUETYPE03-12-2020', 'Image');
define ('GAME0_HINT03-12-2020', 'Company founded by Bill Gates');


define ('GAME0_CLUEIMAGE06-12-2020', 'images/');
define ('GAME0_CLUETEXT06-12-2020', 'The three clues below point to the real first, middle and last name with of this famous actor. How do we know him better as?

First name – shared by the singer of the song "Tears in Heaven"
Middle name – famous actor from The Godfather
Last name – a chess piece 
');
define ('GAME0_CLUETITLE06-12-2020', 'Jamie Foxx');
define ('GAME0_ANSWER06-12-2020', 'Jamie Foxx;Jamie Fox');
define ('GAME0_TRIVIA06-12-2020', 'Foxx first told jokes at a comedy club’s open mic night in 1989, after accepting a girlfriend’s dare. When he found that female comedians were often called first to perform, he changed his name to Jamie Foxx, feeling that it was a name ambiguous enough to disallow any biases');
define ('GAME0_ANSWERIMAGE06-12-2020', 'images/Jamie_Foxx.jpg');
define ('GAME0_ANSWERQUOTE06-12-2020', 'Awesome!');
define ('GAME0_FORMALANSWER06-12-2020', 'Jamie Foxx');
define ('GAME0_CLUETYPE06-12-2020', 'Image');
define ('GAME0_HINT06-12-2020', 'He acted in Django unchained');


define ('GAME0_CLUEIMAGE13-12-2020', 'images/D-SS.jpg');
define ('GAME0_CLUETEXT13-12-2020', 'Identify the character from the Harry Potter series from the following anagram');
define ('GAME0_CLUETITLE13-12-2020', 'Snape');
define ('GAME0_ANSWER13-12-2020', 'Severus Snape;Snape');
define ('GAME0_TRIVIA13-12-2020', 'Snape was the only death eater who could conjure a patronus');
define ('GAME0_ANSWERIMAGE13-12-2020', 'images/Snape.jpg');
define ('GAME0_ANSWERQUOTE13-12-2020', 'Superb!');
define ('GAME0_FORMALANSWER13-12-2020', 'Severus Snape');
define ('GAME0_CLUETYPE13-12-2020', 'Image');
define ('GAME0_HINT13-12-2020', 'Head of Slytherin House');


define ('GAME7_CLUEIMAGE0', 'images/G7C0image.jpg');
define ('GAME7_CLUETEXT0', 'You meet a Greek hero with Amnesia who wants your help in rescuing someone by solving a set of riddles and puzzles . The clues to solve are either greek gods or demi gods from Mythological Greece.');
define ('GAME7_CLUETITLE0', 'Want to play with a Greek God ?');
define ('GAME7_ANSWER0', 'ds');
define ('GAME7_TRIVIA0', 'sd');
define ('GAME7_ANSWERIMAGE0', 'images/s');
define ('GAME7_ANSWERQUOTE0', 'sd');
define ('GAME7_FORMALANSWER0', 'sd');
define ('GAME7_CLUETYPE0', 'Image');
define ('GAME7_HINT0', 'sa');


define ('GAME7_CLUEIMAGE1', 'images/G7C1image.png');
define ('GAME7_CLUETEXT1', 'You run into a lost looking Greek God .He had a painting with him and kept saying . I didnt look at her , that´s why I am alive.');
define ('GAME7_CLUETITLE1', 'I cant remember who I am but I know I have to urgently help someone');
define ('GAME7_ANSWER1', 'Perseus;Persues;Perses');
define ('GAME7_TRIVIA1', 'Perseus trivia here');
define ('GAME7_ANSWERIMAGE1', 'images/G7A1image.jpg');
define ('GAME7_ANSWERQUOTE1', 'Keep my first and sixth letters');
define ('GAME7_FORMALANSWER1', 'Perseus');
define ('GAME7_CLUETYPE1', 'Image');
define ('GAME7_HINT1', 'Google who the image is and use');


define ('GAME7_CLUEIMAGE2', 'images/G7C2image.jpg');
define ('GAME7_CLUETEXT2', 'Of course , I am Perseus . But I dont know who I should rescue . But its urgent , please help me.I remember that she rises at the dark and you can meet her at 00h 42m 44.3s ');
define ('GAME7_CLUETITLE2', 'Now Perseus wants help to find someone');
define ('GAME7_ANSWER2', 'Andromeda;andromeda');
define ('GAME7_TRIVIA2', 'Andromeda trivia');
define ('GAME7_ANSWERIMAGE2', 'images/G7A2image.jpg');
define ('GAME7_ANSWERQUOTE2', 'Keep my fifth and sixth letters');
define ('GAME7_FORMALANSWER2', 'Andromeda');
define ('GAME7_CLUETYPE2', 'Image');
define ('GAME7_HINT2', 'Hint for clue 2');


define ('GAME7_CLUEIMAGE3', 'images/G7C3image.jpg');
define ('GAME7_CLUETEXT3', 'Thanks for helping me remember to find Andromeda . I know someone who can help us find her. He would have said "I Could eat you and then wash it down with some milk and I am hidden in this  box"');
define ('GAME7_CLUETITLE3', 'I know someone who may know where she is .');
define ('GAME7_ANSWER3', 'Cyclops; Cyclopes');
define ('GAME7_TRIVIA3', 'Trivia for Cyclops');
define ('GAME7_ANSWERIMAGE3', 'images/G7A3image.jpg');
define ('GAME7_ANSWERQUOTE3', 'Keep my second and fourth letters');
define ('GAME7_FORMALANSWER3', 'Cyclops');
define ('GAME7_CLUETYPE3', 'Image');
define ('GAME7_HINT3', 'Put the capital letters in their right place - you get the name of a street. Figure out which famous commercial building is on this street');


define ('GAME7_CLUEIMAGE4', 'images/G7Clue4.m4a');
define ('GAME7_CLUETEXT4', 'The Cyclops laughed eerily and said , Listen to this tape and find the main who can take you closer to Andromeda. Dont hope to come back , he leered.');
define ('GAME7_CLUETITLE4', 'And now you fear for your life');
define ('GAME7_ANSWER4', 'Charon');
define ('GAME7_TRIVIA4', 'Trivia for Charon');
define ('GAME7_ANSWERIMAGE4', 'images/G7A4image.jpg');
define ('GAME7_ANSWERQUOTE4', 'Keep my fifth letters');
define ('GAME7_FORMALANSWER4', 'Charon');
define ('GAME7_CLUETYPE4', 'Audio');
define ('GAME7_HINT4', 'Hint for CLue 3');


define ('GAME7_CLUEIMAGE5', 'images/G7C5image.png');
define ('GAME7_CLUETEXT5', 'Charon refused to help or even let you on the boat . But your brush with death isnt over yet. As you walk around listlessly , you run into Sysiphus');
define ('GAME7_CLUETITLE5', 'Sisyphus can help you. I think....');
define ('GAME7_ANSWER5', 'Thanatos');
define ('GAME7_TRIVIA5', 'Trivia for Thanatos');
define ('GAME7_ANSWERIMAGE5', 'images/G7A5image.jpg');
define ('GAME7_ANSWERQUOTE5', 'Keep my fourth and eight letters');
define ('GAME7_FORMALANSWER5', 'Thanatos');
define ('GAME7_CLUETYPE5', 'Image');
define ('GAME7_HINT5', 'Hint Text');


define ('GAME7_CLUEIMAGE6', 'images/G7C6image.png');
define ('GAME7_CLUETEXT6', 'Thanatos at least didnt want to kill us , Perseus said. He gave use the scraps and said it will help us if we can it in the right order');
define ('GAME7_CLUETITLE6', 'Put these piece in the right order');
define ('GAME7_ANSWER6', 'Artemis');
define ('GAME7_TRIVIA6', 'trivia for artemis');
define ('GAME7_ANSWERIMAGE6', 'images/G7A6image.jpg');
define ('GAME7_ANSWERQUOTE6', 'Take my third and fifth letters and tell Sundar what the final location of Andromeda is');
define ('GAME7_FORMALANSWER6', '');
define ('GAME7_CLUETYPE6', 'Image');
define ('GAME7_HINT6', 'Hint forlast one');


