<?php
require_once "config.php";

// ensure that the user is logged in
session_start();

// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login_game.php");
    exit;
}

// get the header file for uniformity
require('header.php');

// create a box to enter a code that lets generation of report happen
$servername = DB_SERVER;
$username = DB_NAME;
$password = DB_PASSWORD;
$dbname = DB_NAME;

// make a hash out of the server params
$serverParams ["servername"] = $servername;
$serverParams ["username"] = $username;
$serverParams ["password"] = $password;
$serverParams ["dbname"] = $dbname;


$getAllPlayDataSQL = "select * from game_plays";


// open DB connection and run the query
    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    //Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
        
    }

    $result = $conn->query($getAllPlayDataSQL);

    ?>

<div class="row">
<div class="col-xs-4 col-md-4">
    <div class="dropdown">
      <button type="button" class="btn btn-danger dropdown-toggle" data-toggle="dropdown">Button</button>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Action</a></li>
            <li><a href="#">Action</a></li>
            <li><a href="#">Action</a></li>
          </ul>
    </div>
</div>


<table id="dtBasicExample" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
<?php while( $row = $result->fetch_assoc() ) { ?>
<tr>
    <td><?php echo $row['PlayID']; ?></td>
    <td><?php echo $row['GameNo']; ?></td>
    <td><?php echo $row['ClueNo']; ?></td>
    <td><?php echo $row['RightAnswer']; ?></td>
    <td><?php echo $row['AnswerGiven']; ?></td>

    <td><?php echo $row['Time']; ?></td>
    <td><?php echo $row['UserID']; ?></td>
    <td><?php echo $row['timetaken']; ?></td>
    
</tr>

<?php } ?>
  
</table>





<?php require('footer.php'); ?>




