
<?php


require_once "config.php";
require_once "game_config.php";

$gameNo = $_GET['game'];
$clueNo = $_GET['clue'];
$noOfClues = $_GET['qty'];

$clueString = constant("GAME".$gameNo."_CLUETEXT".$clueNo);
$clueTitle = constant("GAME".$gameNo."_CLUETITLE".$clueNo);
$clueImage = constant("GAME".$gameNo."_CLUEIMAGE".$clueNo);
$clueType = constant("GAME".$gameNo."_CLUETYPE".$clueNo);
$hint = constant("GAME".$gameNo."_HINT".$clueNo);
$gameTitle = constant("GAME".$gameNo."_CLUETITLE0");
$gameDesc = constant("GAME".$gameNo."_CLUETEXT0");

$hint = "Hint :".$hint ;
/* Handling the daily logic . Need to check if the gameNo says daily and if so , dont set the user session 
( people can play dailies annonymously). 

*/

// if the game is set to zero , its a daily. This is a provison kept for Dailies 
if ($gameNo != 0){
	// Initialize the session
	session_start();
	// Check if the user is logged in, if not then redirect him to login page
	if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
		header("location: login_game.php");	
		exit;		
	}
	$loggedName = htmlspecialchars($_SESSION["username"]); 
}

// Populate the data around the clue based on what type it is - Video , Audio, Image or Text

if ($clueType == "Image"){ 
    $divValue =  "<img src=\"".$clueImage."\" class=\" col-lg-12 img-fluid\">" ;
}
else if ($clueType == "Audio") {
  $divValue =  "<div class=\"card\"><audio controls src=\"".$clueImage."\"> Your browser does not support the <code>audio</code> element. </audio></div>" ;
}
else if ($clueType == "Video") {
  $divValue =  "<div class=\"card\"><video class=\"responsive-video\" controls><source src=\"".$clueImage."\" type=\"video/mp4\"></video></div>" ;
}
else if ($clueType == "Text") {
  $divValue =  "<h1 class=\"display-4\">".$clueImage."</h1>" ;
}
else {
	$divValue = "<div class=\"card-image\"> <img src=\"".$clueImage."\"> <span class=\"card-title\">Clue 7</span></div> " ;
}

// incremenet the next clue 
$nextClue =$clueNo +1 ;
?>

<!-- Header.php should come here to add all data upto end of teh header tag -->
<?php require('header.php'); ?>

<!-- end of the incliude of header.php-->
	<!-- game page -->
	<section class="game_page_section">

		<div class="container"> <!-- Container starts-->
			<div class="row">
				<!-- HEADING Directly in container-->
				<div class="heading_text" data-aos="fade-up">
					<h5><?php echo $gameTitle ; ?></h5>
				</div>
			</div>

			 <!-- CLUENOs and TOTAL NO of CLUES -Directly in container-->
			 <div class="row">
				<div class="col-6">
					<h2 ><?php echo $clueNo; ?> /<?php echo $noOfClues; ?> Clues</h2>
				</div>

				<div id="timer" class="col-6">
						<h6 class="text_yellow">12</h6>
				</div>
			</div>

			<!-- TIMER-Directly in container-->
			
			<!-- CLUE DESCRIPTION-Directly in container-->
			<div id="textForClue" class="clue_description">
				<h5 class="text_yellow">Clue Description</h5>
				<p> <?php echo $clueString; ?></p>
			</div>
			<!-- HINT TEXT --->
			<div id="hintText" class="game_img_text">
				<input type="hidden" name="hint" id="hint" value="<?php echo $hint; ?>">
					<p></p>
					<br>
			</div>

			<!-- GAME SETCION CLASS STARTS-Directly in container-->
			<div class="game_section" data-aos="flip-up">

				<!-- CLASS INSIDE -Directly in Game section class-->
				<div class="row flex-row-reverse">	

					
					<!--Directly in class="row flex-row-reverse"-->
					 <div class="col-lg-12">

						<!-- GETS IMAGE/VIDEO/TEXT of teh CLUE -Directly in class="col-lg-5"-->
						<div id="clueImg1" class="game_images">
							<?php echo $divValue; ?>
							<!--	<img src="images/Mask Group 6.png" class="img-fluid"> -->
						</div>

						<!--Inside =class="col-lg-5"-->	
						<div class="game_subtext ">
							<div class="form-row flex-row-reverse">
								<div class="col-sm-6 col-lg-12">
									<div id="answers" class="answers">
										<h5>Attempted Answers :</h5>
									</div>
								</div>
							</div>
						</div>

						<!-- GETS FORM of teh CLUE -Directly in class="col-lg-5"-->
						<form class="col s12 m8 l6" >
						<br/>
							
							<input type="textarea" class="form-control" id="textarea1"  placeholder="Type your answer" required />
							
							<script>
								document.getElementById('textarea1').addEventListener('keypress', function(event) {
									if (event.keyCode == 13) {
										alert("Please enter a value and hit the Check Answer button");
										event.preventDefault();
									}
								});
							</script>
					
							<input type="hidden" name="user" id="user" value="<?php echo $loggedName; ?>">
							<input type="hidden" name="clue1" id="clueNo" value="<?php echo $clueNo; ?>">
							<input type="hidden" name="game" id="gameNo" value="<?php echo $gameNo; ?>">
							<input type="hidden" name="noOfClues" id="noOfClues" value="<?php echo $noOfClues ; ?>">

							<div id="cluechecker" class="game_img_btn my-4">
								<a href="#" id="hintButton" class="hint yellow_color" onclick="getHint()">Show Hint</a>
								<a href="#" id="cluebutton1" required class="yellow_color question" >Check Answer</a>
								<a href="#" id="skipToAnswer" class="hint yellow_color" >Show Answer</a>
							</div>
						</form>
						<!-- END OF FORM of teh CLUE -Directly in class="img-fluid"-->

						
					

					</div><!--<END OF class="col-lg-5"-->

				
				</div><!--End of  class="row flex-row-reverse"-->	

			</div><!-- End of  class class="game_section" -->

			<!-- NEXTBUTTON-Directly in container-->					
			<div id="nextButton" class="view_btn text-right" >
      			<a href="mainProcessor.php?uname=<?php echo $loggedName; ?>&clue=<?php echo $nextClue; ?>&game=<?php echo $gameNo; ?>&qty=<?php echo $noOfClues; ?>" class="yellow_color">Next </a>
			</div>
			
			<!-- HOMEBUTTON-Directly in container-->	
			<div id="backHomeButton" class="view_btn text-right" >
      			<a href="index.html" class="yellow_color">Back to the home page</a>
			</div>

		</div><!-- END of Container-->
		
	</section>


	<!-- footer.php comes here add the closing body and html tags-->
	<?php require('footer.php'); ?>


