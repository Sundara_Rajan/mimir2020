<!-- Dailies -->
<section class="about_section">
		<div class="container">
			
			<!-- GAME SETCION CLASS STARTS-Directly in container-->
			<div class="game_section" data-aos="flip-up">

			<div class="col-sm-6 game_clues"> 
                    <h2>Daily Mimir : Todays Mimir</h2>        
            </div>

            <div class="col s12 m8 l6 row">
				<!-- CLUENOs and TOTAL NO of CLUES -Directly in container-->
				
				<div class="col-sm-3">
					<h5><span class="text_yellow"><?php echo $clueNo; ?></span> </h5>
				</div>
                <!-- TIMER-Directly in container
                <div class="col-sm-3">
                    <div id="timer" class="seconds text-center">
                        <h2 class="text_yellow">12</h2>
                        <p>Seconds</p>
                    </div>
                </div>-->
            </div>

			<!-- CLUE DESCRIPTION-Directly in container-->
			<div id="textForClue" class="clue_description">
				<h5 class="text_yellow">Clue Description</h5>
				<p> <?php echo $clueString; ?></p>
			</div>
			<!-- HINT TEXT --->
			<div id="hintText" class="game_img_text">
				<input type="hidden" name="hint" id="hint" value="<?php echo $hint; ?>">
					<p></p>
					<br>
			</div>

				<!-- CLASS INSIDE -Directly in Game section class-->
				<div class="row flex-row-reverse">	

					
					<!--Directly in class="row flex-row-reverse"-->
					 <div class="col s12 m8 l6">

						<!-- GETS IMAGE/VIDEO/TEXT of teh CLUE -Directly in class="col-lg-5"-->
						<div id="clueImg1" class="game_images">
							<?php echo $divValue; ?>
							<!--	<img src="images/Mask Group 6.png" class="img-fluid"> -->
						</div>

						<!--Inside =class="col-lg-5"-->	
						<div class="game_subtext ">
							<div class="form-row flex-row-reverse">
								<div class="col-sm-6 col-lg-12">
									<div id="answers" class="answers">
										<h5>Attempted Answers :</h5>
									</div>
								</div>
							</div>
						</div>

						<!-- GETS FORM of teh CLUE -Directly in class="col-lg-5"-->
						<form class="col s12 m8 l6" >
						<br/>
							
							<input type="textarea" class="form-control" id="textarea1"  placeholder="Type your answer" required />
							
							<script>
								document.getElementById('textarea1').addEventListener('keypress', function(event) {
									if (event.keyCode == 13) {
										alert("Please enter a value and hit the Check Answer button");
										event.preventDefault();
									}
								});
							</script>
					
							<input type="hidden" name="user" id="user" value="<?php echo $loggedName; ?>">
							<input type="hidden" name="clue1" id="clueNo" value="<?php echo $clueNo; ?>">
							<input type="hidden" name="game" id="gameNo" value="<?php echo $gameNo; ?>">
							<input type="hidden" name="noOfClues" id="noOfClues" value="<?php echo $noOfClues ; ?>">

							<div id="cluechecker" class="game_img_btn my-4">
								<a href="#" id="hintButton" class="hint yellow_color" onclick="getHint()">Show Hint</a>
								<a href="#" id="skipToAnswer" class="hint yellow_color" >Show Answer</a>
								<a href="#" id="cluebutton1" required class="yellow_color question" >Check Answer</a>
							</div>
						</form>
						<!-- END OF FORM of teh CLUE -Directly in class="img-fluid"-->

						
					

					</div><!--<END OF class="col-lg-5"-->

				
				</div><!--End of  class="row flex-row-reverse"-->	

			</div><!-- End of  class class="game_section" -->
		
		</div><!-- END of Container-->
    </section>