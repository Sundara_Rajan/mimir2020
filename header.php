<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Welcome to Mimir</title>
	<!-- aos -->
	<link href="css/aos.css" rel="stylesheet">	
	<!-- bootstrap -->
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<!-- style.css -->
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>

	<header>
		<nav class="navbar navbar-expand navbar-light">
			<!-- Brand/logo -->
			<a class="navbar-brand logo" href="index.html">
				<img src="images/logo.gif" alt="logo">
			</a>
			<div class="logo_text">
				<ul class="navbar-nav py-2 py-lg-0">
					<li class="nav-item">
						<a class="nav-link" href="#">Todays Mimir</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">All Mimir</a>
					</li>
				</ul>
			</div>
			<ul class="navbar-nav ml-auto py-1 py-lg-0">
				<li class="nav-item">
					<a class="nav-link icon_link login" href="login_game.php">
						<img src="images/enter.png" class="d-block d-lg-none"> Login
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link icon_link" href="#">
						<img src="images/Path 7.png">
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link icon_link" href="#">
						<img src="images/noun_menu_2310205.png">
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link icon_link" href="#">
						<img src="images/noun_redo_2840884.png">
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link icon_link" href="#">
						<img src="images/noun_menu_521074.png">
					</a>
				</li>
			</ul>
		</nav>
	</header>